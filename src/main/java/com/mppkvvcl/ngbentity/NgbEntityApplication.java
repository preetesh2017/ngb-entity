package com.mppkvvcl.ngbentity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NgbEntityApplication {

	public static void main(String[] args) {
		SpringApplication.run(NgbEntityApplication.class, args);
	}
}
