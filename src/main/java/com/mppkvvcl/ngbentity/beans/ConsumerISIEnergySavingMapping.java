package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.ConsumerISIEnergySavingMappingInterface;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by PREETESH on 6/8/2017.
 */
@Entity(name = "ConsumerISIEnergySavingMapping")
@Table(name = "consumer_isi_energy_saving_mapping")
public class ConsumerISIEnergySavingMapping implements ConsumerISIEnergySavingMappingInterface {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "consumer_no")
    private String consumerNo;

    @Column(name = "isi_energy_saving_type")
    private String isiEnergySavingType;

    @Column(name = "created_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "updated_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;

    @Column(name = "updated_by")
    private String updatedBy;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getConsumerNo() {
        return consumerNo;
    }

    public void setConsumerNo(String consumerNo) {
        this.consumerNo = consumerNo;
    }

    public String getIsiEnergySavingType() {
        return isiEnergySavingType;
    }

    public void setIsiEnergySavingType(String isiEnergySavingType) {
        this.isiEnergySavingType = isiEnergySavingType;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Override
    public String toString() {
        return "ConsumerISIEnergySavingMapping{" +
                "id=" + id +
                ", consumerNo='" + consumerNo + '\'' +
                ", isiEnergySavingType='" + isiEnergySavingType + '\'' +
                ", createdOn=" + createdOn +
                ", createdBy='" + createdBy + '\'' +
                ", updatedOn=" + updatedOn +
                ", updatedBy='" + updatedBy + '\'' +
                '}';
    }
}
