package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.TariffDescriptionInterface;

import javax.persistence.*;

/**
 * This is TariffDescription bean class which mapped tariff_description  table in the database
 * Entity use to mapped bean class to given name so that database naming convesion can follow while writting query in spring boot e.g. here Class Tariff represent bean class so
 * first letter is Capital letter but using @Entity (name=tariffCategory) then we can refer table by tariif not by Tariff
 * Table annotation used to mapped table name form backend table name
 * Id represent column as a primary key
 * GeneratedValue represented key generation identity in backend
 * Column mapped table column name with bean member name
 * Created by Girjesh kumar Suryawanshi on 5/19/2017.
 */
@Entity(name = "tariffdescription")
@Table(name = "tariff_description")
public class TariffDescription implements TariffDescriptionInterface {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "tariff_category")
    private String tariffCategory;

    @Column(name = "tariff_description")
    private String tariffDescription;

    public String getTariffDescription() {
        return tariffDescription;
    }

    public void setTariffDescription(String tariffDescription) {
        this.tariffDescription = tariffDescription;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTariffCategory() {
        return tariffCategory;
    }

    public void setTariffCategory(String tariffCategory) {
        this.tariffCategory = tariffCategory;
    }

    @Override
    public String toString() {
        return "TariffDescription{" +
                "id=" + id +
                ", tariffCategory='" + tariffCategory + '\'' +
                ", tariffDescription='" + tariffDescription + '\'' +
                '}';
    }
}
