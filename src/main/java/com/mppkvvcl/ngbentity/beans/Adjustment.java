package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.AdjustmentInterface;
import com.mppkvvcl.ngbinterface.interfaces.AdjustmentTypeInterface;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by SHIVANSHU on 14-07-2017.
 */
@Table(name = "adjustment")
@Entity(name = "Adjustment")
public class Adjustment implements AdjustmentInterface {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "code")
    private int code;

    @OneToOne
    @JoinColumn(name = "code",referencedColumnName = "code",updatable = false,insertable = false)
    private AdjustmentType adjustmentType;

    @Column(name = "consumer_no")
    private String consumerNo;

    @Column(name = "location_code")
    private String locationCode;

    @Column(name = "amount")
    private BigDecimal amount;

    @Column(name = "posted")
    private boolean posted;

    @Column(name = "posting_bill_month")
    private String postingBillMonth;

    @Temporal(TemporalType.DATE)
    @Column(name = "posting_date")
    private Date postingDate;

    @Column(name = "deleted")
    private boolean deleted;

    @Column(name="approval_status")
    private String approvalStatus;

    @Column(name="range_id")
    private long rangeId;

    @Column(name="remark")
    private String remark;

    @Column(name = "created_by")
    private String createdBy;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_on")
    private Date createdOn;

    @Column(name = "updated_by")
    private String updatedBy;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_on")
    private Date updatedOn;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getConsumerNo() {
        return consumerNo;
    }

    public void setConsumerNo(String consumerNo) {
        this.consumerNo = consumerNo;
    }

    public String getLocationCode() {
        return locationCode;
    }

    public void setLocationCode(String locationCode) {
        this.locationCode = locationCode;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public boolean isPosted() {
        return posted;
    }

    public void setPosted(boolean posted) {
        this.posted = posted;
    }

    public String getPostingBillMonth() {
        return postingBillMonth;
    }

    public void setPostingBillMonth(String postingBillMonth) {
        this.postingBillMonth = postingBillMonth;
    }

    public Date getPostingDate() {
        return postingDate;
    }

    public void setPostingDate(Date postingDate) {
        this.postingDate = postingDate;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(String approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    public long getRangeId() {
        return rangeId;
    }

    public void setRangeId(long rangeId) {
        this.rangeId = rangeId;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public AdjustmentTypeInterface getAdjustmentType() {
        return adjustmentType;
    }

    public void setAdjustmentType(AdjustmentTypeInterface adjustmentTypeInterface) {
        if(adjustmentTypeInterface != null && adjustmentTypeInterface instanceof AdjustmentType){
            this.adjustmentType = (AdjustmentType) adjustmentTypeInterface;
        }
    }

    @Override
    public String toString() {

        return "Adjustment{" +
                "id=" + id +
                ", code=" + code +
                ", consumerNo='" + consumerNo + '\'' +
                ", locationCode='" + locationCode + '\'' +
                ", amount=" + amount +
                ", posted=" + posted +
                ", postingBillMonth='" + postingBillMonth + '\'' +
                ", postingDate=" + postingDate +
                ", deleted=" + deleted +
                ", approvalStatus='" + approvalStatus + '\'' +
                ", rangeId=" + rangeId +
                ", remark='" + remark + '\'' +
                ", createdBy='" + createdBy + '\'' +
                ", createdOn=" + createdOn +
                ", updatedBy='" + updatedBy + '\'' +
                ", updatedOn=" + updatedOn +
                '}';
    }
}
