package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.ConsumerMeterMappingInterface;
import com.mppkvvcl.ngbinterface.interfaces.MeterCTRMappingInterface;
import com.mppkvvcl.ngbinterface.interfaces.MeterMasterInterface;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by SUMIT on 20-06-2017.
 *
 * ConsumerMeterMapping for corresponding table
 */
@Entity(name = "ConsumerMeterMapping")
@Table(name = "consumer_meter_mapping")
public class ConsumerMeterMapping implements ConsumerMeterMappingInterface {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "consumer_no")
    private String consumerNo;

    @Column(name = "meter_identifier")
    private String meterIdentifier;

    @Column(name = "meter_serial_no")
    private String meterSerialNo;

    @Column(name = "meter_make")
    private String meterMake;

    @Column(name = "start_read")
    private BigDecimal startRead;

    @Column(name = "final_read")
    private BigDecimal finalRead;

    @Column(name = "mapping_status")
    private String mappingStatus;

    @Column(name = "installation_date")
    @Temporal(TemporalType.DATE)
    private Date installationDate;

    @Column(name = "installation_bill_month")
    private String installationBillMonth;

    @Column(name = "removal_date")
    @Temporal(TemporalType.DATE)
    private Date removalDate;

    @Column(name = "removal_bill_month")
    private String removalBillMonth;

    @Column(name = "remark")
    private String remark;

    @Transient
    private String inactiveRemark;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "created_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;

    @Column(name = "updated_by")
    private String updatedBy;

    @Column(name = "updated_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;

    @OneToOne
    @JoinColumn(name = "meter_identifier",referencedColumnName = "identifier",updatable = false,insertable = false)
    private MeterMaster meterMaster;

    @Transient
    private List<MeterCTRMappingInterface> meterCTRMappings;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getConsumerNo() {
        return consumerNo;
    }

    public void setConsumerNo(String consumerNo) {
        this.consumerNo = consumerNo;
    }

    public String getMeterIdentifier() {
        return meterIdentifier;
    }

    public void setMeterIdentifier(String meterIdentifier) {
        this.meterIdentifier = meterIdentifier;
    }

    public String getMeterSerialNo() {
        return meterSerialNo;
    }

    public void setMeterSerialNo(String meterSerialNo) {
        this.meterSerialNo = meterSerialNo;
    }

    public String getMeterMake() {
        return meterMake;
    }

    public void setMeterMake(String meterMake) {
        this.meterMake = meterMake;
    }

    public BigDecimal getStartRead() {
        return startRead;
    }

    public void setStartRead(BigDecimal startRead) {
        this.startRead = startRead;
    }

    public BigDecimal getFinalRead() {
        return finalRead;
    }

    public void setFinalRead(BigDecimal finalRead) {
        this.finalRead = finalRead;
    }

    public String getMappingStatus() {
        return mappingStatus;
    }

    public void setMappingStatus(String mappingStatus) {
        this.mappingStatus = mappingStatus;
    }

    public Date getInstallationDate() {
        return installationDate;
    }

    public void setInstallationDate(Date installationDate) {
        this.installationDate = installationDate;
    }

    public String getInstallationBillMonth() {
        return installationBillMonth;
    }

    public void setInstallationBillMonth(String installationBillMonth) {
        this.installationBillMonth = installationBillMonth;
    }

    public Date getRemovalDate() {
        return removalDate;
    }

    public void setRemovalDate(Date removalDate) {
        this.removalDate = removalDate;
    }

    public String getRemovalBillMonth() {
        return removalBillMonth;
    }

    public void setRemovalBillMonth(String removalBillMonth) {
        this.removalBillMonth = removalBillMonth;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getInactiveRemark() {
        return inactiveRemark;
    }

    public void setInactiveRemark(String inactiveRemark) {
        this.inactiveRemark = inactiveRemark;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public MeterMasterInterface getMeterMaster() {
        return meterMaster;
    }

    public void setMeterMaster(MeterMasterInterface meterMasterInterface) {
        if(meterMasterInterface != null && meterMasterInterface instanceof MeterMaster){
            this.meterMaster = (MeterMaster) meterMasterInterface;
        }
    }

    public List<MeterCTRMappingInterface> getMeterCTRMappings() {
        return meterCTRMappings;
    }

    public void setMeterCTRMappings(List<MeterCTRMappingInterface> meterCTRMappings) {
        this.meterCTRMappings = meterCTRMappings;
    }

    @Override
    public String toString() {
        return "ConsumerMeterMapping{" +
                "id=" + id +
                ", consumerNo='" + consumerNo + '\'' +
                ", meterIdentifier='" + meterIdentifier + '\'' +
                ", meterSerialNo='" + meterSerialNo + '\'' +
                ", meterMake='" + meterMake + '\'' +
                ", startRead=" + startRead +
                ", finalRead=" + finalRead +
                ", mappingStatus='" + mappingStatus + '\'' +
                ", installationDate=" + installationDate +
                ", installationBillMonth='" + installationBillMonth + '\'' +
                ", removalDate=" + removalDate +
                ", removalBillMonth='" + removalBillMonth + '\'' +
                ", remark='" + remark + '\'' +
                ", inactiveRemark='" + inactiveRemark + '\'' +
                ", createdBy='" + createdBy + '\'' +
                ", createdOn=" + createdOn +
                ", updatedBy='" + updatedBy + '\'' +
                ", updatedOn=" + updatedOn +
                ", meterMaster=" + meterMaster +
                ", meterCTRMappings=" + meterCTRMappings +
                '}';
    }
}

