package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.OICZoneMappingInterface;
import com.mppkvvcl.ngbinterface.interfaces.UserDetailInterface;
import com.mppkvvcl.ngbinterface.interfaces.ZoneInterface;

import javax.persistence.*;

@Entity(name = "OICZoneMapping")
@Table(name = "oic_zone_mapping")
public class OICZoneMapping implements OICZoneMappingInterface {

    @Id
    @Column(name= "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @OneToOne
    @JoinColumn(name = "username",referencedColumnName = "username")
    private UserDetail userDetail;

    @OneToOne
    @JoinColumn(name = "zone_id",referencedColumnName = "id")
    private Zone zone;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public UserDetailInterface getUserDetail() {
        return userDetail;
    }

    public void setUserDetail(UserDetailInterface userDetailInterface) {
        if(userDetailInterface != null && userDetailInterface instanceof UserDetail){
            this.userDetail = (UserDetail)userDetailInterface;
        }
    }

    public ZoneInterface getZone() {
        return zone;
    }

    public void setZone(ZoneInterface zoneInterface) {
        if(zoneInterface != null && zoneInterface instanceof Zone){
            this.zone = (Zone)zoneInterface;
        }
    }
}
