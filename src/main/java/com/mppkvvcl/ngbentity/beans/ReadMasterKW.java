package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.ReadMasterKWInterface;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by PREETESH on 6/29/2017.
 */
@Entity(name = "ReadMasterKW")
@Table(name = "read_master_kw")
public class ReadMasterKW implements ReadMasterKWInterface {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "read_master_id")
    private long readMasterId;

    @Column(name = "meter_md")
    private BigDecimal meterMD;

    @Column(name = "multiplied_md")
    private BigDecimal multipliedMD;

    @Column(name = "billing_demand")
    private BigDecimal billingDemand;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "created_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;

    @Column(name = "updated_by")
    private String updatedBy;

    @Column(name = "updated_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getReadMasterId() {
        return readMasterId;
    }

    public void setReadMasterId(long readMasterId) {
        this.readMasterId = readMasterId;
    }

    public BigDecimal getMeterMD() {
        return meterMD;
    }

    public void setMeterMD(BigDecimal meterMD) {
        this.meterMD = meterMD;
    }

    public BigDecimal getMultipliedMD() {return multipliedMD;  }

    public void setMultipliedMD(BigDecimal multipliedMD) { this.multipliedMD = multipliedMD; }

    public BigDecimal getBillingDemand() {
        return billingDemand;
    }

    public void setBillingDemand(BigDecimal billingDemand) {
        this.billingDemand = billingDemand;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    @Override
    public String toString() {
        return "ReadMasterKW{" +
                "id=" + id +
                ", readMasterId=" + readMasterId +
                ", meterMD=" + meterMD +
                ", multipliedMD=" + multipliedMD +
                ", billingDemand=" + billingDemand +
                ", createdBy='" + createdBy + '\'' +
                ", createdOn=" + createdOn +
                ", updatedBy='" + updatedBy + '\'' +
                ", updatedOn=" + updatedOn +
                '}';
    }

    public ReadMasterKW(){}

    public ReadMasterKW(long id,long readMasterId,BigDecimal meterMD,BigDecimal multipliedMD,BigDecimal billingDemand,String createdBy,Date createdOn,String updatedBy,Date updatedOn){
        this.id = id;
        this.readMasterId = readMasterId;
        this.meterMD = meterMD;
        this.multipliedMD = multipliedMD;
        this.billingDemand = billingDemand;
        this.billingDemand = billingDemand;
        this.createdBy = createdBy;
        this.createdOn = createdOn;
        this.updatedBy = updatedBy;
        this.updatedOn = updatedOn;
    }
}
