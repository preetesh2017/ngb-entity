package com.mppkvvcl.ngbentity.beans;


import com.mppkvvcl.ngbinterface.interfaces.DiscomInterface;
import com.mppkvvcl.ngbinterface.interfaces.RegionInterface;

import javax.persistence.*;

/**
 * Created by MITHLESH on 18-07-2017.
 */
@Entity(name = "Region")
@Table(name = "region")
public class Region implements RegionInterface {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "name")
    private String name;

    /*@Column(name = "discom_id")
    private long discomId;*/

    @OneToOne
    @JoinColumn(name = "discom_id")
    private Discom discom;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /*public long getDiscomId() {
        return discomId;
    }

    public void setDiscomId(long discomId) {
        this.discomId = discomId;
    }*/

    public DiscomInterface getDiscom() {
        return discom;
    }

    public void setDiscom(DiscomInterface discom) {
        if(discom != null && discom instanceof Discom){
            this.discom = (Discom)discom;
        }
    }
}
