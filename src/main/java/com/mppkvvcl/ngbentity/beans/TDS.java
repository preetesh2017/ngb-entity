package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.TDSInterface;

import javax.persistence.*;
import java.math.BigDecimal;


@Table(name = "tds")
@Entity(name = "TDS")
public class TDS implements TDSInterface {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "security_deposit_interest_id")
    private long securityDepositInterestId;

    @Column(name = "amount")
    private BigDecimal amount;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getSecurityDepositInterestId() {
        return securityDepositInterestId;
    }

    public void setSecurityDepositInterestId(long securityDepositInterestId) {
        this.securityDepositInterestId = securityDepositInterestId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "Tds{" +
                "id=" + id +
                ", securityDepositInterestId=" + securityDepositInterestId +
                ", amount=" + amount +
                '}';
    }
}
