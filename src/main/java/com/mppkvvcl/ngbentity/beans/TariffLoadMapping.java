package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.TariffLoadMappingInterface;

import javax.persistence.*;

@Table(name = "tariff_load_mapping")
@Entity(name = "TariffLoadMapping")
public class TariffLoadMapping implements TariffLoadMappingInterface{

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "tariff_detail_id")
    private long tariffDetailId;

    @Column(name = "load_detail_id")
    private long loadDetailId;

    @Column(name = "is_tariff_change")
    private boolean isTariffChange;

    @Column(name = "is_load_change")
    private boolean isLoadChange;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getTariffDetailId() {
        return tariffDetailId;
    }

    public void setTariffDetailId(long tariffDetailId) {
        this.tariffDetailId = tariffDetailId;
    }

    public long getLoadDetailId() {
        return loadDetailId;
    }

    public void setLoadDetailId(long loadDetailId) {
        this.loadDetailId = loadDetailId;
    }

    public boolean isTariffChange() {
        return isTariffChange;
    }

    public void setTariffChange(boolean tariffChange) {
        isTariffChange = tariffChange;
    }

    public boolean isLoadChange() {
        return isLoadChange;
    }

    public void setLoadChange(boolean loadChange) {
        isLoadChange = loadChange;
    }

    @Override
    public String toString() {
        return "TariffLoadMapping{" +
                "id=" + id +
                ", tariffDetailId=" + tariffDetailId +
                ", loadDetailId=" + loadDetailId +
                ", isTariffChange=" + isTariffChange +
                ", isLoadChange=" + isLoadChange +
                '}';
    }
}
