package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.DivisionInterface;
import com.mppkvvcl.ngbinterface.interfaces.EEDivisionMappingInterface;
import com.mppkvvcl.ngbinterface.interfaces.UserDetailInterface;

import javax.persistence.*;

@Entity(name = "EEDivisionMapping")
@Table(name = "ee_division_mapping")
public class EEDivisionMapping implements EEDivisionMappingInterface {

    @Id
    @Column(name= "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @OneToOne
    @JoinColumn(name = "username",referencedColumnName = "username")
    private UserDetail userDetail;

    @OneToOne
    @JoinColumn(name = "division_id",referencedColumnName = "id")
    private Division division;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public UserDetailInterface getUserDetail() {
        return userDetail;
    }

    public void setUserDetail(UserDetailInterface userDetailInterface) {
        if(userDetailInterface != null && userDetailInterface instanceof UserDetail){
            this.userDetail = (UserDetail)userDetailInterface;
        }
    }

    public DivisionInterface getDivision() {
        return division;
    }

    public void setDivision(DivisionInterface divisionInterface) {
        if(divisionInterface != null && divisionInterface instanceof Division){
            this.division = (Division)divisionInterface;
        }
    }
}
