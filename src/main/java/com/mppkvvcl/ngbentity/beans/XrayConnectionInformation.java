package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.XrayConnectionInformationInterface;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by PREETESH on 6/8/2017.
 */
@Entity(name = "XrayConnectionInformation")
@Table(name = "xray_connection_information")
public class XrayConnectionInformation implements XrayConnectionInformationInterface {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "consumer_no")
    private String consumerNo;

    @Column(name = "xray_load")
    private BigDecimal xrayLoad;

    @Column(name = "no_of_dental_xray_machine")
    private long noOfDentalXrayMachine;

    @Column(name = "no_of_single_phase_xray_machine")
    private long noOfSinglePhaseXrayMachine;

    @Column(name = "no_of_three_phase_xray_machine")
    private long noOfThreePhaseXrayMachine;

    @Column(name = "status")
    private String status;

    @Column(name = "created_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "updated_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;

    @Column(name = "updated_by")
    private String updatedBy;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getConsumerNo() {
        return consumerNo;
    }

    public void setConsumerNo(String consumerNo) {
        this.consumerNo = consumerNo;
    }

    public BigDecimal getXrayLoad() {
        return xrayLoad;
    }

    public void setXrayLoad(BigDecimal xrayLoad) {
        this.xrayLoad = xrayLoad;
    }

    public long getNoOfDentalXrayMachine() {
        return noOfDentalXrayMachine;
    }

    public void setNoOfDentalXrayMachine(long noOfDentalXrayMachine) {
        this.noOfDentalXrayMachine = noOfDentalXrayMachine;
    }

    public long getNoOfSinglePhaseXrayMachine() {
        return noOfSinglePhaseXrayMachine;
    }

    public void setNoOfSinglePhaseXrayMachine(long noOfSinglePhaseXrayMachine) {
        this.noOfSinglePhaseXrayMachine = noOfSinglePhaseXrayMachine;
    }

    public long getNoOfThreePhaseXrayMachine() {
        return noOfThreePhaseXrayMachine;
    }

    public void setNoOfThreePhaseXrayMachine(long noOfThreePhaseXrayMachine) {
        this.noOfThreePhaseXrayMachine = noOfThreePhaseXrayMachine;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Override
    public String toString() {
        return "XrayConnectionInformation{" +
                "id=" + id +
                ", consumerNo='" + consumerNo + '\'' +
                ", xrayLoad=" + xrayLoad +
                ", noOfDentalXrayMachine=" + noOfDentalXrayMachine +
                ", noOfSinglePhaseXrayMachine=" + noOfSinglePhaseXrayMachine +
                ", noOfThreePhaseXrayMachine=" + noOfThreePhaseXrayMachine +
                ", status='" + status + '\'' +
                ", createdOn=" + createdOn +
                ", createdBy='" + createdBy + '\'' +
                ", updatedOn=" + updatedOn +
                ", updatedBy='" + updatedBy + '\'' +
                '}';
    }
}
