package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.MeterMakeInterface;

import javax.persistence.*;

/**
 * Created by SHIVANSHU on 18-07-2017.
 */
@Entity(name = "MeterMake")
@Table(name = "meter_make")
public class MeterMake implements MeterMakeInterface {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "make")
    private String make;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    @Override
    public String toString() {
        return "MeterMake{" +
                "id=" + id +
                ", make='" + make + '\'' +
                '}';
    }


}
