package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.CircleInterface;
import com.mppkvvcl.ngbinterface.interfaces.SECircleMappingInterface;
import com.mppkvvcl.ngbinterface.interfaces.UserDetailInterface;

import javax.persistence.*;

@Entity(name = "SECircleMapping")
@Table(name = "se_circle_mapping")
public class SECircleMapping implements SECircleMappingInterface {

    @Id
    @Column(name= "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @OneToOne
    @JoinColumn(name = "username",referencedColumnName = "username")
    private UserDetail userDetail;

    @OneToOne
    @JoinColumn(name = "circle_id",referencedColumnName = "id")
    private Circle circle;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public UserDetailInterface getUserDetail() {
        return userDetail;
    }

    public void setUserDetail(UserDetailInterface userDetailInterface) {
        if(userDetailInterface != null && userDetailInterface instanceof UserDetail){
            this.userDetail = (UserDetail)userDetailInterface;
        }
    }

    public CircleInterface getCircle() {
        return circle;
    }

    public void setCircle(CircleInterface circleInterface) {
        if(circleInterface != null && circleInterface instanceof Circle){
            this.circle =(Circle)circleInterface;
        }
    }
}
