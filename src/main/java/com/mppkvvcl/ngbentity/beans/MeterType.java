package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.MeterTypeInterface;

import javax.persistence.*;

/**
 * Created by SUMIT on 20-06-2017.
 */
@Entity(name = "MeterType")
@Table(name = "meter_type")
public class MeterType implements MeterTypeInterface {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "meter_description")
    private String meterDescription;

    @Column(name = "meter_phase")
    private String meterPhase;

    @Column(name = "meter_code")
    private String meterCode;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMeterDescription() {
        return meterDescription;
    }

    public void setMeterDescription(String meterDescription) {
        this.meterDescription = meterDescription;
    }

    public String getMeterPhase() {
        return meterPhase;
    }

    public void setMeterPhase(String meterPhase) {
        this.meterPhase = meterPhase;
    }

    public String getMeterCode() {
        return meterCode;
    }

    public void setMeterCode(String meterCode) {
        this.meterCode = meterCode;
    }

    @Override
    public String toString() {
        return "MeterType{" +
                "id=" + id +
                ", meterDescription='" + meterDescription + '\'' +
                ", meterPhase='" + meterPhase + '\'' +
                ", meterCode='" + meterCode + '\'' +
                '}';
    }
}
