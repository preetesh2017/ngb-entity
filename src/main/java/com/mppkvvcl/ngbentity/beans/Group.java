package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.GroupInterface;

import javax.persistence.*;

/**
 * Created by PREETESH on 9/1/2017.
 */
@Entity(name = "Group")
@Table(name = "groups")
public class Group implements GroupInterface {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id")
    private long id;

    @Column(name = "group_no")
    private String groupNo;

    @Column(name = "location_code")
    private String locationCode;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getGroupNo() {
        return groupNo;
    }

    public void setGroupNo(String groupNo) {
        this.groupNo = groupNo;
    }

    public String getLocationCode() {
        return locationCode;
    }

    public void setLocationCode(String locationCode) {
        this.locationCode = locationCode;
    }

    @Override
    public String toString() {
        return "Group{" +
                "id=" + id +
                ", groupNo='" + groupNo + '\'' +
                ", locationCode='" + locationCode + '\'' +
                '}';
    }
}
