package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.AdjustmentProfileInterface;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by ANKIT on 18-08-2017.
 */
@Table(name = "adjustment_profile")
@Entity(name = "AdjustmentProfile")
public class AdjustmentProfile implements AdjustmentProfileInterface {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name ="adjustment_id")
    private long adjustmentId;

    @Column(name = "status")
    private boolean status;

    @Column(name = "approver")
    private String approver;

    @Column(name = "location_code")
    private String locationCode;

    @Column(name = "remark")
    private String remark;

    @Column(name = "updated_by")
    private String updatedBy;

    @Column(name = "updated_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getAdjustmentId() {
        return adjustmentId;
    }

    public void setAdjustmentId(long adjustmentId) {
        this.adjustmentId = adjustmentId;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getApprover() {
        return approver;
    }

    public void setApprover(String approver) {
        this.approver = approver;
    }

    public String getLocationCode() {
        return locationCode;
    }

    public void setLocationCode(String locationCode) {
        this.locationCode = locationCode;
    }

    public String getRemark() { return remark; }

    public void setRemark(String remark) { this.remark = remark; }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    @Override
    public String toString() {
        return "AdjustmentProfile{" +
                "id=" + id +
                ", adjustmentId=" + adjustmentId +
                ", status=" + status +
                ", approver='" + approver + '\'' +
                ", locationCode='" + locationCode + '\'' +
                ", remark='" + remark + '\'' +
                ", updatedOn=" + updatedOn +
                '}';
    }
}
