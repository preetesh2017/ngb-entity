package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.LocalHolidayInterface;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by PREETESH on 9/11/2017.
 */
@Entity(name = "LocalHoliday")
@Table(name = "local_holiday")
public class LocalHoliday implements LocalHolidayInterface {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id")
    private long id;

    @Column(name = "location_code")
    private String locationCode;

    @Column(name = "date")
    @Temporal(TemporalType.DATE)
    private Date date;

    @Column(name = "description")
    private String description;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLocationCode() {
        return locationCode;
    }

    public void setLocationCode(String locationCode) {
        this.locationCode = locationCode;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "LocalHoliday{" +
                "id=" + id +
                ", locationCode='" + locationCode + '\'' +
                ", date=" + date +
                ", description='" + description + '\'' +
                '}';
    }
}
