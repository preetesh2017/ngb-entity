package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.CTRMasterInterface;
import com.mppkvvcl.ngbinterface.interfaces.MeterMasterInterface;
import com.mppkvvcl.ngbinterface.interfaces.NSCStagingInterface;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by PREETESH on 5/31/2017.
 */
@Entity(name = "nscstaging")
@Table(name = "nsc_staging")
public class NSCStaging implements NSCStagingInterface {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "connection_date")
    @Temporal(TemporalType.DATE)
    private Date connectionDate;

    @Column(name = "consumer_name")
    private String consumerName;

    @Column(name = "consumer_name_h")
    private String consumerNameH;

    @Column(name = "relative_name")
    private String relativeName;

    @Column(name = "relation")
    private String relation;

    @Column(name = "is_bpl")
    private boolean isBPL;

    @Column(name = "bpl_no")
    private String bplNo;

    @Column(name = "category")
    private String category;

    @Column(name = "is_employee")
    private boolean isEmployee;

    @Column(name = "employee_company")
    private String employeeCompany;

    @Column(name = "employee_no")
    private String employeeNo;

    @Column(name = "address_1")
    private String address1;

    @Column(name = "address_2")
    private String address2;

    @Column(name = "address_3")
    private String address3;

    @Column(name = "address_1_h")
    private String address1H;

    @Column(name = "address_2_h")
    private String address2H;

    @Column(name = "address_3_h")
    private String address3H;

    @Column(name = "primary_mobile_no")
    private String primaryMobileNo;

    @Column(name = "alternate_mobile_no")
    private String alternateMobileNo;

    @Column(name = "aadhaar_no")
    private String aadhaarNo;

    @Column(name = "pan")
    private String pan;

    @Column(name = "bank_account_no")
    private String bankAccountNo;

    @Column(name = "bank_account_holder_name")
    private String bankAccountHolderName;

    @Column(name = "bank_name")
    private String bankName;

    @Column(name = "ifsc")
    private String ifsc;

    @Column(name = "email_address")
    private String emailAddress;

    @Column(name = "tariff_category")
    private String tariffCategory;

    @Column(name = "connection_type")
    private String connectionType;

    @Column(name = "metering_status")
    private String meteringStatus;

    @Column(name = "premise_type")
    private String premiseType;

    @Column(name = "sanctioned_load")
    private BigDecimal sanctionedLoad;

    @Column(name = "sanctioned_load_unit")
    private String sanctionedLoadUnit;

    @Column(name = "contract_demand")
    private BigDecimal contractDemand;

    @Column(name = "contract_demand_unit")
    private String contractDemandUnit;

    @Column(name = "is_seasonal")
    private boolean isSeasonal;

    @Column(name = "season_start_date")
    @Temporal(TemporalType.DATE)
    private Date seasonStartDate;

    @Column(name = "season_end_date")
    @Temporal(TemporalType.DATE)
    private Date seasonEndDate;

    @Column(name = "season_start_bill_month")
    private String seasonStartBillMonth;

    @Column(name = "season_end_bill_month")
    private String seasonEndBillMonth;

    @Column(name = "purpose_of_installation_id")
    private Long purposeOfInstallationId;

    @Column(name = "purpose_of_installation")
    private String purposeOfInstallation;

    @Column(name = "tariff_code")
    private String tariffCode;

    @Column(name = "sub_category_code")
    private long subCategoryCode;

    @Column(name = "phase")
    private String phase;

    @Column(name = "tc_start_date")
    @Temporal(TemporalType.DATE)
    private Date tcStartDate;

    @Column(name = "tc_end_date")
    @Temporal(TemporalType.DATE)
    private Date tcEndDate;

    @Column(name = "is_government")
    private boolean isGovernment;

    @Column(name = "government_type")
    private String governmentType;

    @Column(name = "plot_size")
    private BigDecimal plotSize;

    @Column(name = "plot_size_unit")
    private String plotSizeUnit;

    @Column(name = "location_code")
    private String locationCode;

    @Column(name = "is_xray")
    private boolean isXray;

    @Column(name = "xray_load")
    private BigDecimal xrayLoad;

    @Column(name = "no_of_dental_xray_machine")
    private long noOfDentalXrayMachine;

    @Column(name = "no_of_single_phase_xray_machine")
    private long noOfSinglePhaseXrayMachine;

    @Column(name = "no_of_three_phase_xray_machine")
    private long noOfThreePhaseXrayMachine;

    @Column(name = "is_welding_transformer_surcharge")
    private boolean isWeldingTransformerSurcharge;

    @Column(name = "is_capacitor_surcharge")
    private boolean isCapacitorSurcharge;

    @Column(name = "is_demandside")
    private boolean isDemandside;

    @Column(name = "isi_motor_type")
    private String isiMotorType;

    @Column(name = "is_beneficiary")
    private boolean isBeneficiary;

    @Column(name = "dtr_name")
    private String dtrName;

    @Column(name = "pole_no")
    private String poleNo;

    @Column(name = "pole_latitude")
    private String poleLatitude;

    @Column(name = "pole_longitude")
    private String poleLongitude;

    @Column(name = "feeder_name")
    private String feederName;

    @Column(name = "pole_distance")
    private long poleDistance;

    @Column(name = "area_status")
    private String areaStatus;

    @Column(name = "group_no")
    private String groupNo;

    @Column(name = "reading_diary_no")
    private String readingDiaryNo;

    @Column(name = "neighbour_connection_no")
    private String neighbourConnectionNo;

    @Column(name = "survey_date")
    @Temporal(TemporalType.DATE)
    private Date surveyDate;

    @Column(name = "meter_identifier")
    private String meterIdentifier;

    @OneToOne
    @JoinColumn(name = "meter_identifier", referencedColumnName = "identifier",updatable = false,insertable = false)
    private MeterMaster meterMaster;

    @Column(name = "start_read")
    private BigDecimal startRead;

    @Column(name = "has_ctr")
    private boolean hasCTR;

    @Column(name = "ctr_identifier")
    private String ctrIdentifier;

    @OneToOne
    @JoinColumn(name = "ctr_identifier", referencedColumnName = "identifier",updatable = false,insertable = false)
    private CTRMaster ctrMaster;

    @Column(name = "ctr_overall_mf")
    private BigDecimal ctrOverallMF;

    @Column(name = "meter_installation_date")
    @Temporal(TemporalType.DATE)
    private Date meterInstallationDate;

    @Column(name = "meter_installer_name")
    private String meterInstallerName;

    @Column(name = "meter_installer_designation")
    private String meterInstallerDesignation;

    @Column(name = "has_modem")
    private boolean hasModem;

    @Column(name = "modem_no")
    private String modemNo;

    @Column(name = "sim_no")
    private String simNo;

    @Column(name = "date_of_registration")
    @Temporal(TemporalType.DATE)
    private Date dateOfRegistration;

    @Column(name = "registration_fee_amount")
    private BigDecimal registrationFeeAmount;

    @Column(name = "registration_fee_amount_mr_no")
    private String registrationFeeAmountMRNo;

    @Column(name = "security_deposit_amount")
    private BigDecimal securityDepositAmount;

    @Column(name = "security_deposit_amount_mr_no")
    private String securityDepositAmountMRNo;

    @Column(name = "is_affiliated_consumer")
    private boolean isAffiliatedConsumer;

    @Column(name = "affiliated_consumer_no")
    private String affiliatedConsumerNo;

    @Column(name = "portal_name")
    private String portalName;

    @Column(name = "portal_reference_no")
    private String portalReferenceNo;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "created_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getConnectionDate() {
        return connectionDate;
    }

    public void setConnectionDate(Date connectionDate) {
        this.connectionDate = connectionDate;
    }

    public String getConsumerName() {
        return consumerName;
    }

    public void setConsumerName(String consumerName) {
        this.consumerName = consumerName;
    }

    public String getConsumerNameH() {
        return consumerNameH;
    }

    public void setConsumerNameH(String consumerNameH) {
        this.consumerNameH = consumerNameH;
    }

    public String getRelativeName() {
        return relativeName;
    }

    public void setRelativeName(String relativeName) {
        this.relativeName = relativeName;
    }

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    public boolean getIsBPL() {
        return isBPL;
    }

    public void setIsBPL(boolean isBPL) {
        this.isBPL = isBPL;
    }

    public String getBplNo() {
        return bplNo;
    }

    public void setBplNo(String bplNo) {
        this.bplNo = bplNo;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public boolean getIsEmployee() {
        return isEmployee;
    }

    public void setIsEmployee(boolean isEmployee) {
        this.isEmployee = isEmployee;
    }

    public String getEmployeeCompany() {
        return employeeCompany;
    }

    public void setEmployeeCompany(String employeeCompany) {
        this.employeeCompany = employeeCompany;
    }

    public String getEmployeeNo() {
        return employeeNo;
    }

    public void setEmployeeNo(String employeeNo) {
        this.employeeNo = employeeNo;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress3() {
        return address3;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    public String getAddress1H() {
        return address1H;
    }

    public void setAddress1H(String address1H) {
        this.address1H = address1H;
    }

    public String getAddress2H() {
        return address2H;
    }

    public void setAddress2H(String address2H) {
        this.address2H = address2H;
    }

    public String getAddress3H() {
        return address3H;
    }

    public void setAddress3H(String address3H) {
        this.address3H = address3H;
    }

    public String getPrimaryMobileNo() {
        return primaryMobileNo;
    }

    public void setPrimaryMobileNo(String primaryMobileNo) {
        this.primaryMobileNo = primaryMobileNo;
    }

    public String getAlternateMobileNo() {
        return alternateMobileNo;
    }

    public void setAlternateMobileNo(String alternateMobileNo) {
        this.alternateMobileNo = alternateMobileNo;
    }

    public String getAadhaarNo() {
        return aadhaarNo;
    }

    public void setAadhaarNo(String aadhaarNo) {
        this.aadhaarNo = aadhaarNo;
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public String getBankAccountNo() {
        return bankAccountNo;
    }

    public void setBankAccountNo(String bankAccountNo) {
        this.bankAccountNo = bankAccountNo;
    }

    public String getBankAccountHolderName() {
        return bankAccountHolderName;
    }

    public void setBankAccountHolderName(String bankAccountHolderName) {
        this.bankAccountHolderName = bankAccountHolderName;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getIfsc() {
        return ifsc;
    }

    public void setIfsc(String ifsc) {
        this.ifsc = ifsc;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getTariffCategory() {
        return tariffCategory;
    }

    public void setTariffCategory(String tariffCategory) {
        this.tariffCategory = tariffCategory;
    }

    public String getConnectionType() {
        return connectionType;
    }

    public void setConnectionType(String connectionType) {
        this.connectionType = connectionType;
    }

    public String getMeteringStatus() {
        return meteringStatus;
    }

    public void setMeteringStatus(String meteringStatus) {
        this.meteringStatus = meteringStatus;
    }

    public String getPremiseType() {
        return premiseType;
    }

    public void setPremiseType(String premiseType) {
        this.premiseType = premiseType;
    }

    public BigDecimal getSanctionedLoad() {
        return sanctionedLoad;
    }

    public void setSanctionedLoad(BigDecimal sanctionedLoad) {
        this.sanctionedLoad = sanctionedLoad;
    }

    public String getSanctionedLoadUnit() {
        return sanctionedLoadUnit;
    }

    public void setSanctionedLoadUnit(String sanctionedLoadUnit) {
        this.sanctionedLoadUnit = sanctionedLoadUnit;
    }

    public BigDecimal getContractDemand() {
        return contractDemand;
    }

    public void setContractDemand(BigDecimal contractDemand) {
        this.contractDemand = contractDemand;
    }

    public String getContractDemandUnit() {
        return contractDemandUnit;
    }

    public void setContractDemandUnit(String contractDemandUnit) {
        this.contractDemandUnit = contractDemandUnit;
    }

    public boolean getIsSeasonal() {
        return isSeasonal;
    }

    public void setIsSeasonal(boolean isSeasonal) {
        this.isSeasonal = isSeasonal;
    }

    public Date getSeasonStartDate() {
        return seasonStartDate;
    }

    public void setSeasonStartDate(Date seasonStartDate) {
        this.seasonStartDate = seasonStartDate;
    }

    public Date getSeasonEndDate() {
        return seasonEndDate;
    }

    public void setSeasonEndDate(Date seasonEndDate) {
        this.seasonEndDate = seasonEndDate;
    }

    public String getSeasonStartBillMonth() {
        return seasonStartBillMonth;
    }

    public void setSeasonStartBillMonth(String seasonStartBillMonth) {
        this.seasonStartBillMonth = seasonStartBillMonth;
    }

    public String getSeasonEndBillMonth() {
        return seasonEndBillMonth;
    }

    public void setSeasonEndBillMonth(String seasonEndBillMonth) {
        this.seasonEndBillMonth = seasonEndBillMonth;
    }

    public Long getPurposeOfInstallationId() {
        return purposeOfInstallationId;
    }

    public void setPurposeOfInstallationId(Long purposeOfInstallationId) {
        this.purposeOfInstallationId = purposeOfInstallationId;
    }

    public String getPurposeOfInstallation() {
        return purposeOfInstallation;
    }

    public void setPurposeOfInstallation(String purposeOfInstallation) {
        this.purposeOfInstallation = purposeOfInstallation;
    }

    public String getTariffCode() {
        return tariffCode;
    }

    public void setTariffCode(String tariffCode) {
        this.tariffCode = tariffCode;
    }

    public long getSubCategoryCode() {
        return subCategoryCode;
    }

    public void setSubCategoryCode(long subCategoryCode) {
        this.subCategoryCode = subCategoryCode;
    }

    public String getPhase() {
        return phase;
    }

    public void setPhase(String phase) {
        this.phase = phase;
    }

    public Date getTcStartDate() {
        return tcStartDate;
    }

    public void setTcStartDate(Date tcStartDate) {
        this.tcStartDate = tcStartDate;
    }

    public Date getTcEndDate() {
        return tcEndDate;
    }

    public void setTcEndDate(Date tcEndDate) {
        this.tcEndDate = tcEndDate;
    }

    public boolean getIsGovernment() {
        return isGovernment;
    }

    public void setIsGovernment(boolean isGovernment) {
        this.isGovernment = isGovernment;
    }

    public String getGovernmentType() {
        return governmentType;
    }

    public void setGovernmentType(String governmentType) {
        this.governmentType = governmentType;
    }

    public BigDecimal getPlotSize() {
        return plotSize;
    }

    public void setPlotSize(BigDecimal plotSize) {
        this.plotSize = plotSize;
    }

    public String getPlotSizeUnit() {
        return plotSizeUnit;
    }

    public void setPlotSizeUnit(String plotSizeUnit) {
        this.plotSizeUnit = plotSizeUnit;
    }

    public String getLocationCode() {
        return locationCode;
    }

    public void setLocationCode(String locationCode) {
        this.locationCode = locationCode;
    }

    public boolean getIsXray() {
        return isXray;
    }

    public void setIsXray(boolean isXray) {
        this.isXray = isXray;
    }

    public BigDecimal getXrayLoad() {
        return xrayLoad;
    }

    public void setXrayLoad(BigDecimal xrayLoad) {
        this.xrayLoad = xrayLoad;
    }

    public long getNoOfDentalXrayMachine() {
        return noOfDentalXrayMachine;
    }

    public void setNoOfDentalXrayMachine(long noOfDentalXrayMachine) {
        this.noOfDentalXrayMachine = noOfDentalXrayMachine;
    }

    public long getNoOfSinglePhaseXrayMachine() {
        return noOfSinglePhaseXrayMachine;
    }

    public void setNoOfSinglePhaseXrayMachine(long noOfSinglePhaseXrayMachine) {
        this.noOfSinglePhaseXrayMachine = noOfSinglePhaseXrayMachine;
    }

    public long getNoOfThreePhaseXrayMachine() {
        return noOfThreePhaseXrayMachine;
    }

    public void setNoOfThreePhaseXrayMachine(long noOfThreePhaseXrayMachine) {
        this.noOfThreePhaseXrayMachine = noOfThreePhaseXrayMachine;
    }

    public boolean getIsWeldingTransformerSurcharge() {
        return isWeldingTransformerSurcharge;
    }

    public void setIsWeldingTransformerSurcharge(boolean isWeldingTransformerSurcharge) {
        this.isWeldingTransformerSurcharge = isWeldingTransformerSurcharge;
    }

    public boolean getIsCapacitorSurcharge() {
        return isCapacitorSurcharge;
    }

    public void setIsCapacitorSurcharge(boolean isCapacitorSurcharge) {
        this.isCapacitorSurcharge = isCapacitorSurcharge;
    }

    public boolean getIsDemandside() {
        return isDemandside;
    }

    public void setIsDemandside(boolean isDemandside) {
        this.isDemandside = isDemandside;
    }

    public String getIsiMotorType() {
        return isiMotorType;
    }

    public void setIsiMotorType(String isiMotorType) {
        this.isiMotorType = isiMotorType;
    }

    public boolean getIsBeneficiary() {
        return isBeneficiary;
    }

    public void setIsBeneficiary(boolean isBeneficiary) {
        this.isBeneficiary = isBeneficiary;
    }

    public String getDtrName() {
        return dtrName;
    }

    public void setDtrName(String dtrName) {
        this.dtrName = dtrName;
    }

    public String getPoleNo() {
        return poleNo;
    }

    public void setPoleNo(String poleNo) {
        this.poleNo = poleNo;
    }

    public String getPoleLatitude() {
        return poleLatitude;
    }

    public void setPoleLatitude(String poleLatitude) {
        this.poleLatitude = poleLatitude;
    }

    public String getPoleLongitude() {
        return poleLongitude;
    }

    public void setPoleLongitude(String poleLongitude) {
        this.poleLongitude = poleLongitude;
    }

    public String getFeederName() {
        return feederName;
    }

    public void setFeederName(String feederName) {
        this.feederName = feederName;
    }

    public long getPoleDistance() {
        return poleDistance;
    }

    public void setPoleDistance(long poleDistance) {
        this.poleDistance = poleDistance;
    }

    public String getAreaStatus() {
        return areaStatus;
    }

    public void setAreaStatus(String areaStatus) {
        this.areaStatus = areaStatus;
    }

    public String getGroupNo() {
        return groupNo;
    }

    public void setGroupNo(String groupNo) {
        this.groupNo = groupNo;
    }

    public String getReadingDiaryNo() {
        return readingDiaryNo;
    }

    public void setReadingDiaryNo(String readingDiaryNo) {
        this.readingDiaryNo = readingDiaryNo;
    }

    public String getNeighbourConnectionNo() {
        return neighbourConnectionNo;
    }

    public void setNeighbourConnectionNo(String neighbourConnectionNo) {
        this.neighbourConnectionNo = neighbourConnectionNo;
    }

    public Date getSurveyDate() {
        return surveyDate;
    }

    public void setSurveyDate(Date surveyDate) {
        this.surveyDate = surveyDate;
    }

    public String getMeterIdentifier() {
        return meterIdentifier;
    }

    public void setMeterIdentifier(String meterIdentifier) {
        this.meterIdentifier = meterIdentifier;
    }

    public BigDecimal getStartRead() {
        return startRead;
    }

    public void setStartRead(BigDecimal startRead) {
        this.startRead = startRead;
    }

    public boolean getHasCTR() {
        return hasCTR;
    }

    public void setHasCTR(boolean hasCTR) {
        this.hasCTR = hasCTR;
    }

    public String getCtrIdentifier() {
        return ctrIdentifier;
    }

    public void setCtrIdentifier(String ctrIdentifier) {
        this.ctrIdentifier = ctrIdentifier;
    }

    public BigDecimal getCtrOverallMF() {
        return ctrOverallMF;
    }

    public void setCtrOverallMF(BigDecimal ctrOverallMF) {
        this.ctrOverallMF = ctrOverallMF;
    }

    public Date getMeterInstallationDate() {
        return meterInstallationDate;
    }

    public void setMeterInstallationDate(Date meterInstallationDate) {
        this.meterInstallationDate = meterInstallationDate;
    }

    public String getMeterInstallerName() {
        return meterInstallerName;
    }

    public void setMeterInstallerName(String meterInstallerName) {
        this.meterInstallerName = meterInstallerName;
    }

    public String getMeterInstallerDesignation() {
        return meterInstallerDesignation;
    }

    public void setMeterInstallerDesignation(String meterInstallerDesignation) {
        this.meterInstallerDesignation = meterInstallerDesignation;
    }

    public boolean getHasModem() {
        return hasModem;
    }

    public void setHasModem(boolean hasModem) {
        this.hasModem = hasModem;
    }

    public String getModemNo() {
        return modemNo;
    }

    public void setModemNo(String modemNo) {
        this.modemNo = modemNo;
    }

    public String getSimNo() {
        return simNo;
    }

    public void setSimNo(String simNo) {
        this.simNo = simNo;
    }

    public Date getDateOfRegistration() {
        return dateOfRegistration;
    }

    public void setDateOfRegistration(Date dateOfRegistration) {
        this.dateOfRegistration = dateOfRegistration;
    }

    public BigDecimal getRegistrationFeeAmount() {
        return registrationFeeAmount;
    }

    public void setRegistrationFeeAmount(BigDecimal registrationFeeAmount) {
        this.registrationFeeAmount = registrationFeeAmount;
    }

    public String getRegistrationFeeAmountMRNo() {
        return registrationFeeAmountMRNo;
    }

    public void setRegistrationFeeAmountMRNo(String registrationFeeAmountMRNo) {
        this.registrationFeeAmountMRNo = registrationFeeAmountMRNo;
    }

    public BigDecimal getSecurityDepositAmount() {
        return securityDepositAmount;
    }

    public void setSecurityDepositAmount(BigDecimal securityDepositAmount) {
        this.securityDepositAmount = securityDepositAmount;
    }

    public String getSecurityDepositAmountMRNo() {
        return securityDepositAmountMRNo;
    }

    public void setSecurityDepositAmountMRNo(String securityDepositAmountMRNo) {
        this.securityDepositAmountMRNo = securityDepositAmountMRNo;
    }

    public boolean getIsAffiliatedConsumer() {
        return isAffiliatedConsumer;
    }

    public void setIsAffiliatedConsumer(boolean isAffiliatedConsumer) {
        this.isAffiliatedConsumer = isAffiliatedConsumer;
    }

    public String getAffiliatedConsumerNo() {
        return affiliatedConsumerNo;
    }

    public void setAffiliatedConsumerNo(String affiliatedConsumerNo) {
        this.affiliatedConsumerNo = affiliatedConsumerNo;
    }

    public String getPortalName() {
        return portalName;
    }

    public void setPortalName(String portalName) {
        this.portalName = portalName;
    }

    public String getPortalReferenceNo() {
        return portalReferenceNo;
    }

    public void setPortalReferenceNo(String portalReferenceNo) {
        this.portalReferenceNo = portalReferenceNo;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public MeterMasterInterface getMeterMasterInterface() {
        return meterMaster;
    }

    public void setMeterMasterInterface(MeterMasterInterface meterMasterInterface) {
        if(meterMasterInterface != null && meterMasterInterface instanceof MeterMasterInterface){
            this.meterMaster = (MeterMaster) meterMasterInterface;
        }
    }

    public CTRMasterInterface getCtrMasterInterface() {
        return ctrMaster;
    }

    public void setCtrMasterInterface(CTRMasterInterface ctrMasterInterface) {
        if(ctrMasterInterface != null && ctrMasterInterface instanceof CTRMaster){
            this.ctrMaster = (CTRMaster) ctrMasterInterface;
        }
    }

    @Override
    public String toString() {
        return "NSCStaging{" +
                "id=" + id +
                ", connectionDate=" + connectionDate +
                ", consumerName='" + consumerName + '\'' +
                ", consumerNameH='" + consumerNameH + '\'' +
                ", relativeName='" + relativeName + '\'' +
                ", relation='" + relation + '\'' +
                ", isBPL=" + isBPL +
                ", bplNo='" + bplNo + '\'' +
                ", category='" + category + '\'' +
                ", isEmployee=" + isEmployee +
                ", employeeCompany='" + employeeCompany + '\'' +
                ", employeeNo='" + employeeNo + '\'' +
                ", address1='" + address1 + '\'' +
                ", address2='" + address2 + '\'' +
                ", address3='" + address3 + '\'' +
                ", address1H='" + address1H + '\'' +
                ", address2H='" + address2H + '\'' +
                ", address3H='" + address3H + '\'' +
                ", primaryMobileNo='" + primaryMobileNo + '\'' +
                ", alternateMobileNo='" + alternateMobileNo + '\'' +
                ", aadhaarNo='" + aadhaarNo + '\'' +
                ", pan='" + pan + '\'' +
                ", bankAccountNo='" + bankAccountNo + '\'' +
                ", bankAccountHolderName='" + bankAccountHolderName + '\'' +
                ", bankName='" + bankName + '\'' +
                ", ifsc='" + ifsc + '\'' +
                ", emailAddress='" + emailAddress + '\'' +
                ", tariffCategory='" + tariffCategory + '\'' +
                ", connectionType='" + connectionType + '\'' +
                ", meteringStatus='" + meteringStatus + '\'' +
                ", premiseType='" + premiseType + '\'' +
                ", sanctionedLoad=" + sanctionedLoad +
                ", sanctionedLoadUnit='" + sanctionedLoadUnit + '\'' +
                ", contractDemand=" + contractDemand +
                ", contractDemandUnit='" + contractDemandUnit + '\'' +
                ", isSeasonal=" + isSeasonal +
                ", seasonStartDate=" + seasonStartDate +
                ", seasonEndDate=" + seasonEndDate +
                ", seasonStartBillMonth='" + seasonStartBillMonth + '\'' +
                ", seasonEndBillMonth='" + seasonEndBillMonth + '\'' +
                ", purposeOfInstallationId=" + purposeOfInstallationId +
                ", purposeOfInstallation='" + purposeOfInstallation + '\'' +
                ", tariffCode='" + tariffCode + '\'' +
                ", subCategoryCode=" + subCategoryCode +
                ", phase='" + phase + '\'' +
                ", tcStartDate=" + tcStartDate +
                ", tcEndDate=" + tcEndDate +
                ", isGovernment=" + isGovernment +
                ", governmentType='" + governmentType + '\'' +
                ", plotSize=" + plotSize +
                ", plotSizeUnit='" + plotSizeUnit + '\'' +
                ", locationCode='" + locationCode + '\'' +
                ", isXray=" + isXray +
                ", xrayLoad=" + xrayLoad +
                ", noOfDentalXrayMachine=" + noOfDentalXrayMachine +
                ", noOfSinglePhaseXrayMachine=" + noOfSinglePhaseXrayMachine +
                ", noOfThreePhaseXrayMachine=" + noOfThreePhaseXrayMachine +
                ", isWeldingTransformerSurcharge=" + isWeldingTransformerSurcharge +
                ", isCapacitorSurcharge=" + isCapacitorSurcharge +
                ", isDemandside=" + isDemandside +
                ", isiMotorType='" + isiMotorType + '\'' +
                ", isBeneficiary=" + isBeneficiary +
                ", dtrName='" + dtrName + '\'' +
                ", poleNo='" + poleNo + '\'' +
                ", poleLatitude='" + poleLatitude + '\'' +
                ", poleLongitude='" + poleLongitude + '\'' +
                ", feederName='" + feederName + '\'' +
                ", poleDistance=" + poleDistance +
                ", areaStatus='" + areaStatus + '\'' +
                ", groupNo='" + groupNo + '\'' +
                ", readingDiaryNo='" + readingDiaryNo + '\'' +
                ", neighbourConnectionNo='" + neighbourConnectionNo + '\'' +
                ", surveyDate=" + surveyDate +
                ", meterIdentifier='" + meterIdentifier + '\'' +
                ", startRead=" + startRead +
                ", hasCTR=" + hasCTR +
                ", ctrIdentifier='" + ctrIdentifier + '\'' +
                ", ctrOverallMF=" + ctrOverallMF +
                ", meterInstallationDate=" + meterInstallationDate +
                ", meterInstallerName='" + meterInstallerName + '\'' +
                ", meterInstallerDesignation='" + meterInstallerDesignation + '\'' +
                ", hasModem=" + hasModem +
                ", modemNo='" + modemNo + '\'' +
                ", simNo='" + simNo + '\'' +
                ", dateOfRegistration=" + dateOfRegistration +
                ", registrationFeeAmount=" + registrationFeeAmount +
                ", registrationFeeAmountMRNo='" + registrationFeeAmountMRNo + '\'' +
                ", securityDepositAmount=" + securityDepositAmount +
                ", securityDepositAmountMRNo='" + securityDepositAmountMRNo + '\'' +
                ", isAffiliatedConsumer=" + isAffiliatedConsumer +
                ", affiliatedConsumerNo='" + affiliatedConsumerNo + '\'' +
                ", portalName='" + portalName + '\'' +
                ", portalReferenceNo='" + portalReferenceNo + '\'' +
                ", createdBy='" + createdBy + '\'' +
                ", createdOn=" + createdOn +
                '}';
    }
}
