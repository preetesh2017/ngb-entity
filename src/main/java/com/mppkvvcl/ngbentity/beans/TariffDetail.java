package com.mppkvvcl.ngbentity.beans;


import com.mppkvvcl.ngbinterface.interfaces.TariffDetailInterface;
import com.mppkvvcl.ngbinterface.interfaces.TariffInterface;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by RUPALI on 27-07-2017.
 * Edited by ANSHIKA corrected the import package of String.
 */
@Entity( name = "TariffDetail")
@Table(name = "tariff_detail")
public class TariffDetail implements TariffDetailInterface {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id")
    private long id;

    @Column(name = "consumer_no")
    private String consumerNo;

    @Column(name = "tariff_code")
    private String tariffCode;

    @Column(name = "subcategory_code")
    private long subcategoryCode;

    @Temporal(TemporalType.DATE)
    @Column(name = "effective_start_date")
    private Date effectiveStartDate;

    @Temporal(TemporalType.DATE)
    @Column(name = "effective_end_date")
    private Date effectiveEndDate;

    @Column(name = "bill_month")
    private String billMonth;

    @Column(name = "created_by")
    private String createdBy;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_on")
    private Date createdOn;

    @Column(name = "updated_by")
    private String updatedBy;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_on")
    private Date updatedOn;

    @Transient
    private List<TariffInterface> tariffInterfaces;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getConsumerNo() {
        return consumerNo;
    }

    public void setConsumerNo(String consumerNo) {
        this.consumerNo = consumerNo;
    }

    public String getTariffCode() {
        return tariffCode;
    }

    public void setTariffCode(String tariffCode) {
        this.tariffCode = tariffCode;
    }

    public long getSubcategoryCode() {
        return subcategoryCode;
    }

    public void setSubcategoryCode(long subcategoryCode) {
        this.subcategoryCode = subcategoryCode;
    }

    public Date getEffectiveStartDate() {
        return effectiveStartDate;
    }

    public void setEffectiveStartDate(Date effectiveStartDate) {
        this.effectiveStartDate = effectiveStartDate;
    }

    public Date getEffectiveEndDate() {
        return effectiveEndDate;
    }

    public void setEffectiveEndDate(Date effectiveEndDate) {
        this.effectiveEndDate = effectiveEndDate;
    }

    public String getBillMonth() {
        return billMonth;
    }

    public void setBillMonth(String billMonth) {
        this.billMonth = billMonth;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    @Override
    public void setTariffInterfaces(List<TariffInterface> tariffInterfaces) {
        this.tariffInterfaces = tariffInterfaces;
    }

    @Override
    public List<TariffInterface> getTariffInterfaces() {
        return this.tariffInterfaces;
    }

    @Override
    public String toString() {
        return "TariffDetail{" +
                "id=" + id +
                ", consumerNo=" + consumerNo +
                ", tariffCode=" + tariffCode +
                ", subcategoryCode=" + subcategoryCode +
                ", effectiveStartDate=" + effectiveStartDate +
                ", effectiveEndDate=" + effectiveEndDate +
                ", billMonth=" + billMonth +
                ", createdBy=" + createdBy +
                ", createdOn=" + createdOn +
                ", updatedBy=" + updatedBy +
                ", updatedOn=" + updatedOn +
                '}';
    }
}