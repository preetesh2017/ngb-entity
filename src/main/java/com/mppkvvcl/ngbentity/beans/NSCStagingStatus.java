package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.NSCStagingInterface;
import com.mppkvvcl.ngbinterface.interfaces.NSCStagingStatusInterface;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by SUMIT on 31-05-2017.
 * This is the entity mapping to the table nsc_staging_status
 */
@Entity(name = "nscstagingstatus")
@Table(name = "nsc_staging_status")
public class NSCStagingStatus implements NSCStagingStatusInterface {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "nsc_staging_id")
    private long nscStagingId;

    @OneToOne
    @JoinColumn(name = "nsc_staging_id",referencedColumnName = "id",updatable = false,insertable = false)
    private NSCStaging nscStaging;

    @Column(name = "status")
    private String status;

    @Column(name = "consumer_no")
    private String consumerNo;

    @Column(name = "remark")
    private String remark;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "created_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;

    @Column(name = "updated_by")
    private String updatedBy;

    @Column(name = "updated_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getNscStagingId() {
        return nscStagingId;
    }

    public void setNscStagingId(long nscStagingId) {
        this.nscStagingId = nscStagingId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getConsumerNo() {
        return consumerNo;
    }

    public void setConsumerNo(String consumerNo) {
        this.consumerNo = consumerNo;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public NSCStagingInterface getNscStagingInterface() {
        return nscStaging;
    }

    public void setNscStagingInterface(NSCStagingInterface nscStagingInterface) {
        if(nscStagingInterface != null && nscStagingInterface instanceof NSCStaging){
            this.nscStaging = (NSCStaging) nscStagingInterface;
        }
    }

    @Override
    public String toString() {
        return "NSCStagingStatus{" +
                "id=" + id +
                ", nscStagingId=" + nscStagingId +
                ", status='" + status + '\'' +
                ", consumerNo='" + consumerNo + '\'' +
                ", remark='" + remark + '\'' +
                ", createdBy='" + createdBy + '\'' +
                ", createdOn=" + createdOn +
                ", updatedBy='" + updatedBy + '\'' +
                ", updatedOn=" + updatedOn +
                '}';
    }
}
