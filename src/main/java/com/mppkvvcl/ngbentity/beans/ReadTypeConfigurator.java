package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.ReadTypeConfiguratorInterface;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by PREETESH on 7/1/2017.
 */
@Entity(name = "ReadTypeConfigurator")
@Table(name = "read_type_configurator")
public class ReadTypeConfigurator implements ReadTypeConfiguratorInterface {

    @Id
    @Column(name= "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "tariff_id")
    private long tariffId;

    @Column(name = "subcategory_code")
    private long subcategoryCode;

    @Column(name = "kwh" )
    private boolean kwh;

    @Column(name = "kw" )
    private boolean kw;

    @Column(name = "pf" )
    private boolean pf;

    @Column(name = "kva" )
    private boolean kva;

    @Column(name = "kvah" )
    private boolean kvah;

    @Column(name = "tod1" )
    private boolean tod1;

    @Column(name = "tod2" )
    private boolean tod2;

    @Column(name = "tod3" )
    private boolean tod3;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "created_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;

    @Column(name = "updated_by")
    private String updatedBy;

    @Column(name = "updated_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getTariffId() {
        return tariffId;
    }

    public void setTariffId(long tariffId) {
        this.tariffId = tariffId;
    }

    public long getSubcategoryCode() {
        return subcategoryCode;
    }

    public void setSubcategoryCode(long subcategoryCode) {
        this.subcategoryCode = subcategoryCode;
    }

    public boolean isKwh() {
        return kwh;
    }

    public void setKwh(boolean kwh) {
        this.kwh = kwh;
    }

    public boolean isKw() {
        return kw;
    }

    public void setKw(boolean kw) {
        this.kw = kw;
    }

    public boolean isPf() {
        return pf;
    }

    public void setPf(boolean pf) {
        this.pf = pf;
    }

    public boolean isKva() {
        return kva;
    }

    public void setKva(boolean kva) {
        this.kva = kva;
    }

    public boolean isKvah() {
        return kvah;
    }

    public void setKvah(boolean kvah) {
        this.kvah = kvah;
    }

    public boolean isTod1() {
        return tod1;
    }

    public void setTod1(boolean tod1) {
        this.tod1 = tod1;
    }

    public boolean isTod2() {
        return tod2;
    }

    public void setTod2(boolean tod2) {
        this.tod2 = tod2;
    }

    public boolean isTod3() {
        return tod3;
    }

    public void setTod3(boolean tod3) {
        this.tod3 = tod3;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    @Override
    public String toString() {
        return "ReadTypeConfigurator{" +
                "id=" + id +
                ", tariffId=" + tariffId +
                ", subcategoryCode=" + subcategoryCode +
                ", kwh=" + kwh +
                ", kw=" + kw +
                ", pf=" + pf +
                ", kva=" + kva +
                ", kvah=" + kvah +
                ", tod1=" + tod1 +
                ", tod2=" + tod2 +
                ", tod3=" + tod3 +
                ", createdBy='" + createdBy + '\'' +
                ", createdOn=" + createdOn +
                ", updatedBy='" + updatedBy + '\'' +
                ", updatedOn=" + updatedOn +
                '}';
    }
}
