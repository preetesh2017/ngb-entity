package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.GovernmentTypeInterface;

import javax.persistence.*;

/**
 * Created by PREETESH on 6/8/2017.
 */
@Entity(name = "GovernmentType")
@Table(name = "government_type")
public class GovernmentType implements GovernmentTypeInterface {


    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "type")
    private String type;

    @Override
    public String toString() {
        return "GovernmentType{" +
                "id=" + id +
                ", type='" + type + '\'' +
                '}';
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
