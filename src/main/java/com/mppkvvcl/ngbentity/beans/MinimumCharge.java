package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.MinimumChargeInterface;

import javax.persistence.*;
import java.math.BigDecimal;

@Table(name = "minimum_charge")
@Entity(name = "MinimumCharge")
public class MinimumCharge implements MinimumChargeInterface{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "tariff_id")
    private long tariffId;

    @Column(name = "subcategory_code")
    private long subcategoryCode;

    @Column(name = "amount")
    private BigDecimal amount;

    @Column(name = "app_version")
    private String appVersion;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getTariffId() {
        return tariffId;
    }

    public void setTariffId(long tariffId) {
        this.tariffId = tariffId;
    }

    public long getSubcategoryCode() {
        return subcategoryCode;
    }

    public void setSubcategoryCode(long subcategoryCode) {
        this.subcategoryCode = subcategoryCode;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    @Override
    public String toString() {
        return "MinimumCharge{" +
                "id=" + id +
                ", tariffId=" + tariffId +
                ", subcategoryCode=" + subcategoryCode +
                ", amount=" + amount +
                ", appVersion='" + appVersion + '\'' +
                '}';
    }
}
