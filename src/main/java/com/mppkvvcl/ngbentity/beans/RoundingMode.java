package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.RoundingModeInterface;

import javax.persistence.*;

@Table(name = "rounding_mode")
@Entity(name = "RoundingMode")
public class RoundingMode implements RoundingModeInterface {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "code")
    private int code;

    @Column(name = "type")
    private String type;

    @Column(name = "description")
    private String  description;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "RoundingMode{" +
                "id=" + id +
                ", code=" + code +
                ", type='" + type + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
