package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.ElectricityDutyInterface;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by ANSHIKA on 07-07-2017.
 */
@Entity(name = "ElectricityDuty")
@Table(name = "electricity_duty")
public class ElectricityDuty implements ElectricityDutyInterface {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "subcategory_code")
    private Long subCategoryCode;

    @Column(name = "start_consumption")
    private Long startConsumption;

    @Column(name = "end_consumption")
    private Long endConsumption;

    @Column(name = "rate")
    private BigDecimal rate;

    @Temporal(TemporalType.DATE)
    @Column(name = "effective_start_date")
    private Date effectiveStartDate;

    @Temporal(TemporalType.DATE)
    @Column(name = "effective_end_date")
    private Date effectiveEndDate;

    @Column(name = "multiplier")
    private BigDecimal multiplier;

    @Column(name = "app_version")
    private String appVersion;

    @Column(name = "created_by")
    private String createdBy;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_on")
    private Date createdOn;

    @Column(name = "updated_by")
    private String updatedBy;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_on")
    private Date updatedOn;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSubcategoryCode() {
        return subCategoryCode;
    }

    public void setSubcategoryCode(Long subCategoryCode) { this.subCategoryCode = subCategoryCode; }

    public Long getStartConsumption() {
        return startConsumption;
    }

    public void setStartConsumption(Long startConsumption) {
        this.startConsumption = startConsumption;
    }

    public Long getEndConsumption() {
        return endConsumption;
    }

    public void setEndConsumption(Long endConsumption) {
        this.endConsumption = endConsumption;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public Date getEffectiveStartDate() {
        return effectiveStartDate;
    }

    public void setEffectiveStartDate(Date effectiveStartDate) {
        this.effectiveStartDate = effectiveStartDate;
    }

    public Date getEffectiveEndDate() {
        return effectiveEndDate;
    }

    public void setEffectiveEndDate(Date effectiveEndDate) {
        this.effectiveEndDate = effectiveEndDate;
    }

    public Long getSubCategoryCode() {
        return subCategoryCode;
    }

    public void setSubCategoryCode(Long subCategoryCode) {
        this.subCategoryCode = subCategoryCode;
    }

    public BigDecimal getMultiplier() {
        return multiplier;
    }

    public void setMultiplier(BigDecimal multiplier) {
        this.multiplier = multiplier;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    @Override
    public String toString() {
        return "ElectricityDuty{" +
                "id=" + id +
                ", subCategoryCode=" + subCategoryCode +
                ", startConsumption=" + startConsumption +
                ", endConsumption=" + endConsumption +
                ", rate=" + rate +
                ", effectiveStartDate=" + effectiveStartDate +
                ", effectiveEndDate=" + effectiveEndDate +
                ", multiplier=" + multiplier +
                ", appVersion='" + appVersion + '\'' +
                ", createdBy='" + createdBy + '\'' +
                ", createdOn=" + createdOn +
                ", updatedBy='" + updatedBy + '\'' +
                ", updatedOn=" + updatedOn +
                '}';
    }
}
