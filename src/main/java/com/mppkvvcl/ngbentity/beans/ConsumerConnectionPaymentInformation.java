package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.ConsumerConnectionPaymentInformationInterface;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by PREETESH on 6/7/2017.
 */

@Entity(name = "ConsumerConnectionPaymentInformation")
@Table(name = "consumer_connection_payment_information")
public class ConsumerConnectionPaymentInformation implements ConsumerConnectionPaymentInformationInterface {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "consumer_no")
    private String consumerNo;

    @Column(name = "date_of_registration")
    @Temporal(TemporalType.DATE)
    private Date dateOfRegistration;

    @Column(name = "registration_fee_amount")
    private BigDecimal registrationFeeAmount;

    @Column(name = "registration_fee_amount_mr_no")
    private String registrationFeeAmountMRNo;

    @Column(name = "security_deposit_amount")
    private BigDecimal securityDepositAmount;

    @Column(name = "security_deposit_amount_mr_no")
    private String securityDepositAmountMRNo;

    @Column(name = "created_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "updated_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;

    @Column(name = "updated_by")
    private String updatedBy;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getConsumerNo() {
        return consumerNo;
    }

    public void setConsumerNo(String consumerNo) {
        this.consumerNo = consumerNo;
    }

    public Date getDateOfRegistration() {
        return dateOfRegistration;
    }

    public void setDateOfRegistration(Date dateOfRegistration) {
        this.dateOfRegistration = dateOfRegistration;
    }

    public BigDecimal getRegistrationFeeAmount() {
        return registrationFeeAmount;
    }

    public void setRegistrationFeeAmount(BigDecimal registrationFeeAmount) {
        this.registrationFeeAmount = registrationFeeAmount;
    }

    public String getRegistrationFeeAmountMRNo() {
        return registrationFeeAmountMRNo;
    }

    public void setRegistrationFeeAmountMRNo(String registrationFeeAmountMRNo) {
        this.registrationFeeAmountMRNo = registrationFeeAmountMRNo;
    }

    public BigDecimal getSecurityDepositAmount() {
        return securityDepositAmount;
    }

    public void setSecurityDepositAmount(BigDecimal securityDepositAmount) {
        this.securityDepositAmount = securityDepositAmount;
    }

    public String getSecurityDepositAmountMRNo() {
        return securityDepositAmountMRNo;
    }

    public void setSecurityDepositAmountMRNo(String securityDepositAmountMRNo) {
        this.securityDepositAmountMRNo = securityDepositAmountMRNo;
    }

    public void setCreatedOn(Date createdOn) { this.createdOn = createdOn; }

    public Date getCreatedOn() { return createdOn; }

    public void setCreatedBy(String createdBy) { this.createdBy = createdBy; }

    public String getCreatedBy() { return createdBy; }

    public void setUpdatedOn(Date updatedOn) { this.updatedOn = updatedOn; }

    public Date getUpdatedOn() { return updatedOn;   }

    public void setUpdatedBy(String updatedBy) { this.updatedBy = updatedBy;  }

    public String getUpdatedBy() { return updatedBy;  }

    @Override
    public String toString() {
        return "ConsumerConnectionPaymentInformation{" +
                "id=" + id +
                ", consumerNo='" + consumerNo + '\'' +
                ", dateOfRegistration=" + dateOfRegistration +
                ", registrationFeeAmount=" + registrationFeeAmount +
                ", registrationFeeAmountMRNo='" + registrationFeeAmountMRNo + '\'' +
                ", securityDepositAmount=" + securityDepositAmount +
                ", securityDepositAmountMRNo='" + securityDepositAmountMRNo + '\'' +
                ", createdOn=" + createdOn +
                ", createdBy='" + createdBy + '\'' +
                ", updatedOn=" + updatedOn +
                ", updatedBy='" + updatedBy + '\'' +
                '}';
    }
}