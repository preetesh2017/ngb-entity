package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.ReadMasterInterface;
import com.mppkvvcl.ngbinterface.interfaces.ReadMasterKWInterface;
import com.mppkvvcl.ngbinterface.interfaces.ReadMasterPFInterface;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * Created by PREETESH on 6/30/2017.
 */
@Entity(name = "ReadMaster")
@Table(name = "read_master")
public class ReadMaster implements ReadMasterInterface {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "bill_month")
    private String billMonth;

    @Column(name = "group_no")
    private String groupNo;

    @Column(name = "reading_diary_no")
    private String readingDiaryNo;

    @Column(name = "consumer_no")
    private String consumerNo;

    @Column(name = "meter_identifier")
    private String meterIdentifier;

    @Column(name = "reading_date")
    @Temporal(TemporalType.DATE)
    private Date readingDate;

    @Column(name = "reading_type")
    private String readingType;

    @Column(name = "meter_status")
    private String meterStatus;

    @Column(name = "replacement_flag")
    private String replacementFlag;

    @Column(name = "source")
    private String source;

    @Column(name = "reading")
    private BigDecimal reading;

    @Column(name = "difference")
    private BigDecimal difference;

    @Column(name = "mf")
    private BigDecimal mf;

    @Column(name = "consumption")
    private BigDecimal consumption;

    @Column(name = "assessment")
    private BigDecimal assessment;

    @Column(name = "propagated_assessment")
    private BigDecimal propagatedAssessment;

    @Column(name = "total_consumption")
    private BigDecimal totalConsumption;

    @Column(name = "used_on_bill")
    private boolean usedOnBill;

    @Transient
    private ReadMasterKWInterface readMasterKW;

    @Transient
    private ReadMasterPFInterface readMasterPF;

    @Transient
    private String  remark;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "created_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;

    @Column(name = "updated_by")
    private String updatedBy;

    @Column(name = "updated_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getBillMonth() {
        return billMonth;
    }

    public void setBillMonth(String billMonth) {
        this.billMonth = billMonth;
    }

    public String getGroupNo() {
        return groupNo;
    }

    public void setGroupNo(String groupNo) {
        this.groupNo = groupNo;
    }

    public String getReadingDiaryNo() {
        return readingDiaryNo;
    }

    public void setReadingDiaryNo(String readingDiaryNo) {
        this.readingDiaryNo = readingDiaryNo;
    }

    public String getConsumerNo() {
        return consumerNo;
    }

    public void setConsumerNo(String consumerNo) {
        this.consumerNo = consumerNo;
    }

    public String getMeterIdentifier() {
        return meterIdentifier;
    }

    public void setMeterIdentifier(String meterIdentifier) {
        this.meterIdentifier = meterIdentifier;
    }

    public Date getReadingDate() {
        return readingDate;
    }

    public void setReadingDate(Date readingDate) {
        this.readingDate = readingDate;
    }

    public String getReadingType() {
        return readingType;
    }

    public void setReadingType(String readingType) {
        this.readingType = readingType;
    }

    public String getMeterStatus() {
        return meterStatus;
    }

    public void setMeterStatus(String meterStatus) {
        this.meterStatus = meterStatus;
    }

    public String getReplacementFlag() {
        return replacementFlag;
    }

    public void setReplacementFlag(String replacementFlag) {
        this.replacementFlag = replacementFlag;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public BigDecimal getReading() {
        return reading;
    }

    public void setReading(BigDecimal reading) {
        this.reading = reading;
    }

    public BigDecimal getDifference() {
        return difference;
    }

    public void setDifference(BigDecimal difference) {
        this.difference = difference;
    }

    public BigDecimal getMf() {
        return mf;
    }

    public void setMf(BigDecimal mf) {
        this.mf = mf;
    }

    public BigDecimal getConsumption() {
        return consumption;
    }

    public void setConsumption(BigDecimal consumption) {
        this.consumption = consumption;
    }

    public BigDecimal getAssessment() {
        return assessment;
    }

    public void setAssessment(BigDecimal assessment) {
        this.assessment = assessment;
    }

    public BigDecimal getPropagatedAssessment() {return propagatedAssessment; }

    public void setPropagatedAssessment(BigDecimal propagatedAssessment) {
        this.propagatedAssessment = propagatedAssessment;
    }

    public BigDecimal getTotalConsumption() {
        return totalConsumption;
    }

    public void setTotalConsumption(BigDecimal totalConsumption) {
        this.totalConsumption = totalConsumption;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public ReadMasterKWInterface getReadMasterKW() {
        return readMasterKW;
    }

    public void setReadMasterKW(ReadMasterKWInterface readMasterKW) {
        this.readMasterKW = readMasterKW;
    }

    public ReadMasterPFInterface getReadMasterPF() {
        return readMasterPF;
    }

    public String getRemark() {return remark;}

    public void setRemark(String remark) {this.remark = remark;}

    public void setReadMasterPF(ReadMasterPFInterface readMasterPF) {
        this.readMasterPF = readMasterPF;
    }

    public boolean isUsedOnBill() {
        return usedOnBill;
    }

    public void setUsedOnBill(boolean usedOnBill) {
        this.usedOnBill = usedOnBill;
    }

    @Override
    public String toString() {
        return "ReadMaster{" +
                "id=" + id +
                ", billMonth='" + billMonth + '\'' +
                ", groupNo='" + groupNo + '\'' +
                ", readingDiaryNo='" + readingDiaryNo + '\'' +
                ", consumerNo='" + consumerNo + '\'' +
                ", meterIdentifier='" + meterIdentifier + '\'' +
                ", readingDate=" + readingDate +
                ", readingType='" + readingType + '\'' +
                ", meterStatus='" + meterStatus + '\'' +
                ", replacementFlag='" + replacementFlag + '\'' +
                ", source='" + source + '\'' +
                ", reading=" + reading +
                ", difference=" + difference +
                ", mf=" + mf +
                ", consumption=" + consumption +
                ", assessment=" + assessment +
                ", propagatedAssessment=" + propagatedAssessment +
                ", totalConsumption=" + totalConsumption +
                ", usedOnBill=" + usedOnBill +
                ", readMasterKW=" + readMasterKW +
                ", readMasterPF=" + readMasterPF +
                ", remark='" + remark + '\'' +
                ", createdBy='" + createdBy + '\'' +
                ", createdOn=" + createdOn +
                ", updatedBy='" + updatedBy + '\'' +
                ", updatedOn=" + updatedOn +
                '}';
    }

    public ReadMaster(){}

    public ReadMaster(long id, String billMonth, String groupNo, String readingDiaryNo, String consumerNo, String meterIdentifier, Date readingDate, String readingType,
                      String meterStatus, String replacementFlag, String source, BigDecimal reading, BigDecimal difference, BigDecimal mf, BigDecimal consumption, BigDecimal assessment,
                      BigDecimal propagatedAssessment, BigDecimal totalConsumption, boolean usedOnBill, String createdBy, Date createdOn, String updatedBy, Date updatedOn,
                      String remark, ReadMasterKWInterface readMasterKWInterface, ReadMasterPFInterface readMasterPFInterface){
        this.id = id;
        this.billMonth = billMonth;
        this.groupNo = groupNo;
        this.readingDiaryNo = readingDiaryNo;
        this.consumerNo = consumerNo;
        this.meterIdentifier = meterIdentifier;
        this.readingDate = readingDate;
        this.readingType = readingType;
        this.meterStatus = meterStatus;
        this.replacementFlag = replacementFlag;
        this.source = source;
        this.reading = reading;
        this.difference = difference;
        this.mf = mf;
        this.consumption = consumption;
        this.assessment = assessment;
        this.propagatedAssessment = propagatedAssessment;
        this.totalConsumption = totalConsumption;
        this.usedOnBill = usedOnBill;
        this.remark = remark;
        this.readMasterKW = readMasterKWInterface;
        this.readMasterPF = readMasterPFInterface;
        this.createdBy = createdBy;
        this.createdOn = createdOn;
        this.updatedBy = updatedBy;
        this.updatedOn = updatedOn;
    }
}

