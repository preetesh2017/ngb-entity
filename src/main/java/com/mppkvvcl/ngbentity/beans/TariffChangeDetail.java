package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.TariffChangeDetailInterface;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by vikas on 8/23/2017.
 */

@Entity(name = "TariffChangeDetail")
@Table(name = "tariff_change_detail")
public class TariffChangeDetail implements TariffChangeDetailInterface {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "tariff_detail_id")
    private long tariffDetailId;

    @Column(name = "purpose_of_installation_id")
    private long purposeOfInstallationId;

    @Column(name = "purpose_of_installation")
    private String purposeOfInstallation;

    @Column(name = "metering_status")
    private String meteringStatus;

    @Column(name = "is_seasonal")
    private boolean isSeasonal;

    @Column(name = "is_xray")
    private boolean isXray;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "created_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;

    @Column(name = "updated_by")
    private String updatedBy;

    @Column(name = "updated_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getTariffDetailId() {
        return tariffDetailId;
    }

    public void setTariffDetailId(long tariffDetailId) {
        this.tariffDetailId = tariffDetailId;
    }

    public long getPurposeOfInstallationId() {
        return purposeOfInstallationId;
    }

    public void setPurposeOfInstallationId(long purposeOfInstallationId) {
        this.purposeOfInstallationId = purposeOfInstallationId;
    }

    public String getPurposeOfInstallation() {
        return purposeOfInstallation;
    }

    public void setPurposeOfInstallation(String purposeOfInstallation) {
        this.purposeOfInstallation = purposeOfInstallation;
    }

    public String getMeteringStatus() {
        return meteringStatus;
    }

    public void setMeteringStatus(String meteringStatus) {
        this.meteringStatus = meteringStatus;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public boolean getIsSeasonal() {
        return isSeasonal;
    }

    public void setIsSeasonal(boolean isSeasonal) {
        this.isSeasonal = isSeasonal;
    }

    public boolean getIsXray() {
        return isXray;
    }

    public void setIsXray(boolean xray) {
        isXray = xray;
    }

    @Override
    public String toString() {
        return "TariffChangeDetail{" +
                "id=" + id +
                ", tariffDetailId=" + tariffDetailId +
                ", purposeOfInstallationId=" + purposeOfInstallationId +
                ", purposeOfInstallation='" + purposeOfInstallation + '\'' +
                ", meteringStatus='" + meteringStatus + '\'' +
                ", isSeasonal=" + isSeasonal +
                ", isXray=" + isXray +
                ", createdBy='" + createdBy + '\'' +
                ", createdOn=" + createdOn +
                ", updatedBy='" + updatedBy + '\'' +
                ", updatedOn=" + updatedOn +
                '}';
    }
}
