package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.MeterRentInterface;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by SUMIT on 20-06-2017.
 */
@Entity(name = "MeterRent")
@Table(name = "meter_rent")
public class MeterRent implements MeterRentInterface {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "meter_code")
    private String meterCode;

    @Column(name = "meter_rent")
    private BigDecimal meterRent;

    @Column(name = "effective_start_date")
    @Temporal(TemporalType.DATE)
    private Date effectiveStartDate;

    @Column(name = "effective_end_date")
    @Temporal(TemporalType.DATE)
    private Date effectiveEndDate;

    @Column(name = "meter_capacity")
    private String meterCapacity;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMeterCode() {
        return meterCode;
    }

    public void setMeterCode(String meterCode) {
        this.meterCode = meterCode;
    }

    public BigDecimal getMeterRent() {
        return meterRent;
    }

    public void setMeterRent(BigDecimal meterRent) {
        this.meterRent = meterRent;
    }

    public Date getEffectiveStartDate() {
        return effectiveStartDate;
    }

    public void setEffectiveStartDate(Date effectiveStartDate) {
        this.effectiveStartDate = effectiveStartDate;
    }

    public Date getEffectiveEndDate() {
        return effectiveEndDate;
    }

    public void setEffectiveEndDate(Date effectiveEndDate) {
        this.effectiveEndDate = effectiveEndDate;
    }

    public String getMeterCapacity() {
        return meterCapacity;
    }

    public void setMeterCapacity(String meterCapacity) {
        this.meterCapacity = meterCapacity;
    }

    @Override
    public String toString() {
        return "MeterRent{" +
                "id=" + id +
                ", meterCode='" + meterCode + '\'' +
                ", meterRent=" + meterRent +
                ", effectiveStartDate=" + effectiveStartDate +
                ", effectiveEndDate=" + effectiveEndDate +
                ", meterCapacity='" + meterCapacity + '\'' +
                '}';
    }
}
