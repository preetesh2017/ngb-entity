package com.mppkvvcl.ngbentity.beans;


import com.mppkvvcl.ngbinterface.interfaces.CircleInterface;
import com.mppkvvcl.ngbinterface.interfaces.DivisionInterface;

import javax.persistence.*;

/**
 * Created by RUPALI on 17-07-2017.
 */
@Entity(name = "Division")
@Table(name = "division")
public class Division implements DivisionInterface {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id")
    private long id;

    @Column(name = "code")
    private String code;

    @Column(name = "name")
    private String name;

    /*@Column(name = "circle_id")
    private long circleId;*/

    @OneToOne
    @JoinColumn(name = "circle_id")
    private Circle circle;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /*public long getCircleId() {
        return circleId;
    }

    public void setCircleId(long circleId) {
        this.circleId = circleId;
    }*/

    public CircleInterface getCircle() {
        return circle;
    }

    public void setCircle(CircleInterface circle) {
        if(circle != null && circle instanceof Circle){
            this.circle = (Circle)circle;
        }
    }
}
