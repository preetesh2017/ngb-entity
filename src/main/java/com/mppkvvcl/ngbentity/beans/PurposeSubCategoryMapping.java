package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.PurposeSubCategoryMappingInterface;

import javax.persistence.*;

/**
 * Created by NITISH on 26-05-2017.
 * This class is bean class for purpose_subcategory_mapping table at the backend database.
 * This bean will hold row data for that table across the app.
 */
@Entity(name = "purposesubcategorymapping")
@Table(name = "purpose_subcategory_mapping")
public class PurposeSubCategoryMapping implements PurposeSubCategoryMappingInterface {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "purpose_of_installation_id")
    private long purposeOfInstallationId;

    @Column(name = "subcategory_code")
    private long subcategoryCode;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getPurposeOfInstallationId() {
        return purposeOfInstallationId;
    }

    public void setPurposeOfInstallationId(long purposeOfInstallationId) {
        this.purposeOfInstallationId = purposeOfInstallationId;
    }

    public long getSubcategoryCode() {
        return subcategoryCode;
    }

    public void setSubcategoryCode(long subcategoryCode) {
        this.subcategoryCode = subcategoryCode;
    }

    @Override
    public String toString() {
        return "PurposeSubCategoryMapping{" +
                "id=" + id +
                ", purposeOfInstallationId=" + purposeOfInstallationId +
                ", subcategoryCode=" + subcategoryCode +
                '}';
    }
}
