package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.PaymentBifurcationPriorityInterface;

import javax.persistence.*;

/**
 * Created by PREETESH on 1/1/2018.
 */
@Entity(name = "PaymentBifurcationPriority")
@Table(name = "payment_bifurcation_priority")
public class PaymentBifurcationPriority implements PaymentBifurcationPriorityInterface {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "head")
    private String head;

    @Column(name = "priority")
    private int priority;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getHead() {
        return head;
    }

    public void setHead(String head) {
        this.head = head;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    @Override
    public String toString() {
        return "PaymentBifurcationPriority{" +
                "id=" + id +
                ", head='" + head + '\'' +
                ", priority=" + priority +
                '}';
    }
}
