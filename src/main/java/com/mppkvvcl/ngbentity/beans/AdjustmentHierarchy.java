package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.AdjustmentHierarchyInterface;
import javax.persistence.*;

/**
 * Created by ANKIT on 18-08-2017.
 */
@Table(name = "adjustment_hierarchy")
@Entity(name = "AdjustmentHierarchy")
public class AdjustmentHierarchy implements AdjustmentHierarchyInterface {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "role")
    private String role;

    @Column(name = "priority")
    private int priority;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    @Override
    public String toString() {
        return "AdjustmentHierarchy{" +
                "id=" + id +
                ", role=" + role +
                ", priority=" + priority +
                '}';
    }
}
