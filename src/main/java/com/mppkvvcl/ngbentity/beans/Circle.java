package com.mppkvvcl.ngbentity.beans;


import com.mppkvvcl.ngbinterface.interfaces.CircleInterface;
import com.mppkvvcl.ngbinterface.interfaces.RegionInterface;

import javax.persistence.*;

/**
     * Created by MITHLESH on 18-07-2017.
 */
@Entity(name = "Circle")
@Table(name = "circle")
public class Circle implements CircleInterface {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "code")
    private String code;

    @Column(name = "name")
    private String name;

    /*@Column(name = "region_id")
    private long regionId;*/

    @OneToOne
    @JoinColumn(name = "region_id")
    private Region region;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RegionInterface getRegion() {
        return region;
    }

    public void setRegion(RegionInterface region) {
        if(region != null && region instanceof Region){
            this.region = (Region)region;
        }
    }
}
