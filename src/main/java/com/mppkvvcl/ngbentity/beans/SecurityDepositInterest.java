package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.SecurityDepositInterestInterface;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by PREETESH on 11/17/2017.
 */
@Entity(name = "security_deposit_interest")
@Table(name = "SecurityDepositInterest")
public class SecurityDepositInterest implements SecurityDepositInterestInterface {

    @Id
    @Column(name= "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "consumer_no")
    private String consumerNo;

    @Column(name = "calculation_start_date")
    @Temporal(TemporalType.DATE)
    private Date calculationStartDate;

    @Column(name = "calculation_end_date")
    @Temporal(TemporalType.DATE)
    private Date calculationEndDate;

    @Column(name = "bill_month" )
    private String billMonth;

    @Column(name = "amount")
    private BigDecimal amount;

    @Column(name = "created_by")
    private String createdBy;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_on")
    private Date createdOn;

    @Column(name = "updated_by")
    private String updatedBy;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_on")
    private Date updatedOn;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getConsumerNo() {
        return consumerNo;
    }

    public void setConsumerNo(String consumerNo) {
        this.consumerNo = consumerNo;
    }

    public Date getCalculationStartDate() {
        return calculationStartDate;
    }

    public void setCalculationStartDate(Date calculationStartDate) {
        this.calculationStartDate = calculationStartDate;
    }

    public Date getCalculationEndDate() {
        return calculationEndDate;
    }

    public void setCalculationEndDate(Date calculationEndDate) {
        this.calculationEndDate = calculationEndDate;
    }

    public String getBillMonth() {
        return billMonth;
    }

    public void setBillMonth(String billMonth) {
        this.billMonth = billMonth;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Override
    public Date getCreatedOn() {
        return createdOn;
    }

    @Override
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Override
    public Date getUpdatedOn() {
        return updatedOn;
    }

    @Override
    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    @Override
    public String toString() {
        return "SecurityDepositInterest{" +
                "id=" + id +
                ", consumerNo='" + consumerNo + '\'' +
                ", calculationStartDate=" + calculationStartDate +
                ", calculationEndDate=" + calculationEndDate +
                ", billMonth='" + billMonth + '\'' +
                ", amount=" + amount +
                ", createdBy='" + createdBy + '\'' +
                ", createdOn=" + createdOn +
                ", updatedBy='" + updatedBy + '\'' +
                ", updatedOn=" + updatedOn +
                '}';
    }
}
