package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.AgricultureBill6MonthlyInterface;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Table(name = "agriculture_bill_6monthly")
@Entity(name = "AgricultureBill6Monthly")
public class AgricultureBill6Monthly implements AgricultureBill6MonthlyInterface {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "consumer_no")
    private String consumerNo;

    @Column(name = "start_bill_month")
    private String startBillMonth;

    @Column(name = "end_bill_month")
    private String endBillMonth;

    @Column(name = "issue_date")
    @Temporal(TemporalType.DATE)
    private Date issueDate;

    @Column(name = "due_date")
    @Temporal(TemporalType.DATE)
    private Date dueDate;

    @Column(name = "cheque_due_date")
    @Temporal(TemporalType.DATE)
    private Date chequeDueDate;

    @Column(name = "load")
    private BigDecimal load;

    @Column(name = "unit")
    private BigDecimal unit;

    @Column(name = "energy_charge")
    private BigDecimal energyCharge;

    @Column(name = "fixed_charge")
    private BigDecimal fixedCharge;

    @Column(name = "fca")
    private BigDecimal fca;

    @Column(name = "capacitor_surcharge")
    private BigDecimal capacitorSurcharge;

    @Column(name = "actual_bill")
    private BigDecimal actualBill;

    @Column(name = "subsidy")
    private BigDecimal subsidy;

    @Column(name = "current_bill")
    private BigDecimal currentBill;

    @Column(name = "security_deposit")
    private BigDecimal securityDeposit;

    @Column(name = "sd_interest")
    private BigDecimal sdInterest;

    @Column(name = "arrear")
    private BigDecimal arrear;

    @Column(name = "old_arrear_installment")
    private BigDecimal oldArrearInstallment;

    @Column(name = "old_arrear_installment_subsidy")
    private BigDecimal oldArrearInstallmentSubsidy;

    @Column(name = "surcharge")
    private BigDecimal surcharge;

    @Column(name = "cumulative_surcharge")
    private BigDecimal cumulativeSurcharge;

    @Column(name = "net_bill")
    private BigDecimal netBill;

    @Column(name = "surcharge_arrear")
    private BigDecimal surchargeArrear;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "created_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;

    @Column(name = "updated_by")
    private String updatedBy;

    @Column(name = "updated_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getConsumerNo() {
        return consumerNo;
    }

    public void setConsumerNo(String consumerNo) {
        this.consumerNo = consumerNo;
    }

    public String getStartBillMonth() {
        return startBillMonth;
    }

    public void setStartBillMonth(String startBillMonth) {
        this.startBillMonth = startBillMonth;
    }

    public String getEndBillMonth() {
        return endBillMonth;
    }

    public void setEndBillMonth(String endBillMonth) {
        this.endBillMonth = endBillMonth;
    }

    public Date getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public Date getChequeDueDate() {
        return chequeDueDate;
    }

    public void setChequeDueDate(Date chequeDueDate) {
        this.chequeDueDate = chequeDueDate;
    }

    public BigDecimal getLoad() {
        return load;
    }

    public void setLoad(BigDecimal load) {
        this.load = load;
    }

    public BigDecimal getUnit() {
        return unit;
    }

    public void setUnit(BigDecimal unit) {
        this.unit = unit;
    }

    public BigDecimal getEnergyCharge() {
        return energyCharge;
    }

    public void setEnergyCharge(BigDecimal energyCharge) {
        this.energyCharge = energyCharge;
    }

    public BigDecimal getFixedCharge() {
        return fixedCharge;
    }

    public void setFixedCharge(BigDecimal fixedCharge) {
        this.fixedCharge = fixedCharge;
    }

    public BigDecimal getFca() {
        return fca;
    }

    public void setFca(BigDecimal fca) {
        this.fca = fca;
    }

    public BigDecimal getCapacitorSurcharge() {
        return capacitorSurcharge;
    }

    public void setCapacitorSurcharge(BigDecimal capacitorSurcharge) {
        this.capacitorSurcharge = capacitorSurcharge;
    }

    public BigDecimal getActualBill() {
        return actualBill;
    }

    public void setActualBill(BigDecimal actualBill) {
        this.actualBill = actualBill;
    }

    public BigDecimal getSubsidy() {
        return subsidy;
    }

    public void setSubsidy(BigDecimal subsidy) {
        this.subsidy = subsidy;
    }

    public BigDecimal getCurrentBill() {
        return currentBill;
    }

    public void setCurrentBill(BigDecimal currentBill) {
        this.currentBill = currentBill;
    }

    public BigDecimal getSecurityDeposit() {
        return securityDeposit;
    }

    public void setSecurityDeposit(BigDecimal securityDeposit) {
        this.securityDeposit = securityDeposit;
    }

    public BigDecimal getSdInterest() {
        return sdInterest;
    }

    public void setSdInterest(BigDecimal sdInterest) {
        this.sdInterest = sdInterest;
    }

    public BigDecimal getArrear() {
        return arrear;
    }

    public void setArrear(BigDecimal arrear) {
        this.arrear = arrear;
    }

    public BigDecimal getOldArrearInstallment() {
        return oldArrearInstallment;
    }

    public void setOldArrearInstallment(BigDecimal oldArrearInstallment) {
        this.oldArrearInstallment = oldArrearInstallment;
    }

    public BigDecimal getOldArrearInstallmentSubsidy() {
        return oldArrearInstallmentSubsidy;
    }

    public void setOldArrearInstallmentSubsidy(BigDecimal oldArrearInstallmentSubsidy) {
        this.oldArrearInstallmentSubsidy = oldArrearInstallmentSubsidy;
    }

    public BigDecimal getSurcharge() {
        return surcharge;
    }

    public void setSurcharge(BigDecimal surcharge) {
        this.surcharge = surcharge;
    }

    public BigDecimal getCumulativeSurcharge() {
        return cumulativeSurcharge;
    }

    public void setCumulativeSurcharge(BigDecimal cumulativeSurcharge) {
        this.cumulativeSurcharge = cumulativeSurcharge;
    }

    public BigDecimal getNetBill() {
        return netBill;
    }

    public void setNetBill(BigDecimal netBill) {
        this.netBill = netBill;
    }

    public BigDecimal getSurchargeArrear() {
        return surchargeArrear;
    }

    public void setSurchargeArrear(BigDecimal surchargeArrear) {
        this.surchargeArrear = surchargeArrear;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    @Override
    public String toString() {
        return "AgricultureBill6Monthly{" +
                "id=" + id +
                ", consumerNo='" + consumerNo + '\'' +
                ", startBillMonth='" + startBillMonth + '\'' +
                ", endBillMonth='" + endBillMonth + '\'' +
                ", issueDate=" + issueDate +
                ", dueDate=" + dueDate +
                ", chequeDueDate=" + chequeDueDate +
                ", load=" + load +
                ", unit=" + unit +
                ", energyCharge=" + energyCharge +
                ", fixedCharge=" + fixedCharge +
                ", fca=" + fca +
                ", capacitorSurcharge=" + capacitorSurcharge +
                ", actualBill=" + actualBill +
                ", subsidy=" + subsidy +
                ", currentBill=" + currentBill +
                ", securityDeposit=" + securityDeposit +
                ", sdInterest=" + sdInterest +
                ", arrear=" + arrear +
                ", oldArrearInstallment=" + oldArrearInstallment +
                ", oldArrearInstallmentSubsidy=" + oldArrearInstallmentSubsidy +
                ", surcharge=" + surcharge +
                ", cumulativeSurcharge=" + cumulativeSurcharge +
                ", netBill=" + netBill +
                ", surchargeArrear=" + surchargeArrear +
                ", createdBy='" + createdBy + '\'' +
                ", createdOn=" + createdOn +
                ", updatedBy='" + updatedBy + '\'' +
                ", updatedOn=" + updatedOn +
                '}';
    }

    public AgricultureBill6Monthly(){
    }

    @Transient
    private final BigDecimal multiplier = new BigDecimal(6);

    public AgricultureBill6Monthly(BigDecimal billedUnit,BigDecimal energyCharge,BigDecimal fixedCharge,BigDecimal fcaCharge,BigDecimal pfSurcharge){
        this.unit = billedUnit.multiply(multiplier);
        this.energyCharge = energyCharge.multiply(multiplier);
        this.fixedCharge = fixedCharge.multiply(multiplier);
        this.fca = fcaCharge.multiply(multiplier);
        this.capacitorSurcharge = pfSurcharge.multiply(multiplier);
        //Setting currentBill as per value
        this.actualBill = this.energyCharge.add(this.fixedCharge).add(this.fca).add(this.capacitorSurcharge);
    }

    public AgricultureBill6Monthly(AgricultureBill6MonthlyInterface agricultureBill6MonthlyInterface){
        if(agricultureBill6MonthlyInterface != null){
            this.id = agricultureBill6MonthlyInterface.getId();
            this.consumerNo = agricultureBill6MonthlyInterface.getConsumerNo();
            this.startBillMonth = agricultureBill6MonthlyInterface.getStartBillMonth();
            this.endBillMonth = agricultureBill6MonthlyInterface.getEndBillMonth();
            this.issueDate = agricultureBill6MonthlyInterface.getIssueDate();
            this.dueDate = agricultureBill6MonthlyInterface.getDueDate();
            this.chequeDueDate = agricultureBill6MonthlyInterface.getChequeDueDate();
            this.load = agricultureBill6MonthlyInterface.getLoad();
            this.unit = agricultureBill6MonthlyInterface.getUnit();
            this.energyCharge = agricultureBill6MonthlyInterface.getEnergyCharge();
            this.fixedCharge = agricultureBill6MonthlyInterface.getFixedCharge();
            this.fca = agricultureBill6MonthlyInterface.getFca();
            this.capacitorSurcharge = agricultureBill6MonthlyInterface.getCapacitorSurcharge();
            this.actualBill = agricultureBill6MonthlyInterface.getActualBill();
            this.subsidy = agricultureBill6MonthlyInterface.getSubsidy();
            this.currentBill = agricultureBill6MonthlyInterface.getCurrentBill();
            this.securityDeposit = agricultureBill6MonthlyInterface.getSecurityDeposit();
            this.sdInterest = agricultureBill6MonthlyInterface.getSdInterest();
            this.arrear = agricultureBill6MonthlyInterface.getArrear();
            this.oldArrearInstallment = agricultureBill6MonthlyInterface.getOldArrearInstallment();
            this.oldArrearInstallmentSubsidy = agricultureBill6MonthlyInterface.getOldArrearInstallmentSubsidy();
            this.surcharge = agricultureBill6MonthlyInterface.getSurcharge();
            this.cumulativeSurcharge = agricultureBill6MonthlyInterface.getCumulativeSurcharge();
            this.netBill = agricultureBill6MonthlyInterface.getNetBill();
            this.surchargeArrear = agricultureBill6MonthlyInterface.getSurchargeArrear();
        }
    }
}
