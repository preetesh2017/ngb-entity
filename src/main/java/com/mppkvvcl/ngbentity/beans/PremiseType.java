package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.PremiseTypeInterface;

import javax.persistence.*;

/**
 * Created by SHIVANSHU on 18-07-2017.
 */

@Entity(name = "PremiseType")
@Table(name = "premise_type")
public class PremiseType implements PremiseTypeInterface {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "type")
    private String type;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "PremiseType{" +
                "id=" + id +
                ", type='" + type + '\'' +
                '}';
    }

}
