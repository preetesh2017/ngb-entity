package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.PurposeOfInstallationInterface;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by PREETESH on 5/19/2017.
 * This Beans is depicting property of connection which describe purpose of installation of connection
 * i.e. description of tariffCategory - Domestic, Industrial, Irrigation etc
 *
 */

@Entity(name = "purposeofinstallation")
@Table(name= "purpose_of_installation")
public class PurposeOfInstallation implements PurposeOfInstallationInterface {

    @Id
    @Column(name= "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name="tariff_code")
    private String tariffCode;

    @Column(name="purpose_of_installation")
    private String purposeOfInstallation;

    @Column(name="load_unit")
    private String loadUnit;

    @Column(name="max_load")
    private BigDecimal maxLoad;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTariffCode() {
        return tariffCode;
    }

    public void setTariffCode(String tariffCode) {
        this.tariffCode = tariffCode;
    }

    public String getPurposeOfInstallation() {
        return purposeOfInstallation;
    }

    public void setPurposeOfInstallation(String purposeOfInstallation) {
        this.purposeOfInstallation = purposeOfInstallation;
    }

    public String getLoadUnit() {
        return loadUnit;
    }

    public void setLoadUnit(String loadUnit) {
        this.loadUnit = loadUnit;
    }

    public BigDecimal getMaxLoad() {
        return maxLoad;
    }

    public void setMaxLoad(BigDecimal maxLoad) {
        this.maxLoad = maxLoad;
    }

    @Override
    public String toString() {
        return "PurposeOfInstallation{" +
                "id=" + id +
                ", tariffCode='" + tariffCode + '\'' +
                ", purposeOfInstallation='" + purposeOfInstallation + '\'' +
                ", loadUnit='" + loadUnit + '\'' +
                ", maxLoad=" + maxLoad +
                '}';
    }
}
