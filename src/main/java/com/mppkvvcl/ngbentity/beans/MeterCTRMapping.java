package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.MeterCTRMappingInterface;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by SUMIT on 20-06-2017.
 */
@Entity(name = "MeterCTRMapping")
@Table(name = "meter_ctr_mapping")
public class MeterCTRMapping implements MeterCTRMappingInterface {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "meter_identifier")
    private String meterIdentifier;

    @Column(name = "ctr_identifier")
    private String ctrIdentifier;

    @Column(name = "overall_mf")
    private BigDecimal overallMf;

    @Column(name = "mapping_status")
    private String mappingStatus;

    @Column(name = "installation_date")
    @Temporal(TemporalType.DATE)
    private Date installationDate;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "created_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;

    @Column(name = "updated_by")
    private String updatedBy;

    @Column(name = "updated_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMeterIdentifier() {
        return meterIdentifier;
    }

    public void setMeterIdentifier(String meterIdentifier) {
        this.meterIdentifier = meterIdentifier;
    }

    public String getCtrIdentifier() {
        return ctrIdentifier;
    }

    public void setCtrIdentifier(String ctrIdentifier) {
        this.ctrIdentifier = ctrIdentifier;
    }

    public BigDecimal getOverallMf() {
        return overallMf;
    }

    public void setOverallMf(BigDecimal overallMf) {
        this.overallMf = overallMf;
    }

    public String getMappingStatus() {
        return mappingStatus;
    }

    public void setMappingStatus(String mappingStatus) {
        this.mappingStatus = mappingStatus;
    }

    public Date getInstallationDate() {
        return installationDate;
    }

    public void setInstallationDate(Date installationDate) {
        this.installationDate = installationDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    @Override
    public String toString() {
        return "MeterCTRMapping{" +
                "id=" + id +
                ", meterIdentifier='" + meterIdentifier + '\'' +
                ", ctrIdentifier='" + ctrIdentifier + '\'' +
                ", overallMf=" + overallMf +
                ", mappingStatus='" + mappingStatus + '\'' +
                ", installationDate=" + installationDate +
                ", createdBy='" + createdBy + '\'' +
                ", createdOn=" + createdOn +
                ", updatedBy='" + updatedBy + '\'' +
                ", updatedOn=" + updatedOn +
                '}';
    }
}
