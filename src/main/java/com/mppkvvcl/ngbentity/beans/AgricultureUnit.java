package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.AgricultureUnitInterface;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by SHIVANSHU on 14-07-2017.
 */
@Entity(name = "AgricultureUnit")
@Table(name = "agriculture_unit")
public class AgricultureUnit implements AgricultureUnitInterface {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "subcategory_code")
    private long subcategoryCode;

    @Column(name = "tariff_id")
    private long tariffId;

    @Column(name = "alternate_subcategory_code")
    private long alternateSubcategoryCode;

    @Column(name = "phase")
    private String phase;

    @Column(name = "area_type")
    private String areaType;

    @Column(name = "description")
    private String description;

    @Column(name = "start_month")
    private String startMonth;

    @Column(name = "end_month")
    private String endMonth;

    @Column(name = "billing_units")
    private BigDecimal billingUnits;

    public long getId() {        return id;    }

    public void setId(long id) {        this.id = id;    }

    public long getSubcategoryCode() {        return subcategoryCode;    }

    public void setSubcategoryCode(long subcategoryCode) {        this.subcategoryCode = subcategoryCode;    }

    public long getTariffId() {
        return tariffId;
    }

    public void setTariffId(long tariffId) {
        this.tariffId = tariffId;
    }

    public long getAlternateSubcategoryCode() {        return alternateSubcategoryCode;    }

    public void setAlternateSubcategoryCode(long alternateSubcategoryCode) {        this.alternateSubcategoryCode = alternateSubcategoryCode;    }

    public String getPhase() {        return phase;    }

    public void setPhase(String phase) {        this.phase = phase;    }

    public String getAreaType() {        return areaType;    }

    public void setAreaType(String areaType) {        this.areaType = areaType;    }

    public String getDescription() {        return description;    }

    public void setDescription(String description) {        this.description = description;    }

    public String getStartMonth() {        return startMonth;    }

    public void setStartMonth(String startMonth) {        this.startMonth = startMonth;    }

    public String getEndMonth() {        return endMonth;    }

    public void setEndMonth(String endMonth) {        this.endMonth = endMonth;    }

    public BigDecimal getBillingUnits() {        return billingUnits;    }

    public void setBillingUnits(BigDecimal billingUnits) {        this.billingUnits = billingUnits;    }


    @Override
    public String toString() {
        return "AgricultureUnit{" +
                "id=" + id +
                ", subcategoryCode=" + subcategoryCode +
                ", tariffId=" + tariffId +
                ", alternateSubcategoryCode=" + alternateSubcategoryCode +
                ", phase='" + phase + '\'' +
                ", areaType='" + areaType + '\'' +
                ", description='" + description + '\'' +
                ", startMonth='" + startMonth + '\'' +
                ", endMonth='" + endMonth + '\'' +
                ", billingUnits=" + billingUnits +
                '}';
    }
}
