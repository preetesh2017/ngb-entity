package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.AdditionalSecurityDepositInterface;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by PREETESH on 11/17/2017.
 */
@Entity(name = "additional_security_deposit")
@Table(name = "AdditionalSecurityDeposit")
public class AdditionalSecurityDeposit implements AdditionalSecurityDepositInterface {


    @Id
    @Column(name= "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "consumer_no")
    private String consumerNo;

    @Column(name = "average_consumption")
    private BigDecimal averageConsumption;

    @Column(name = "average_bill")
    private BigDecimal averageBill;

    @Column(name = "is_defaulter")
    private boolean isDefaulter;

    @Column(name = "bill_month" )
    private String billMonth;

    @Column(name = "security_deposit_demand")
    private BigDecimal securityDepositDemand;

    @Column(name = "period")
    private int period;

    @Column(name = "created_by")
    private String createdBy;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_on")
    private Date createdOn;

    @Column(name = "updated_by")
    private String updatedBy;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_on")
    private Date updatedOn;


    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String getConsumerNo() {
        return consumerNo;
    }

    @Override
    public void setConsumerNo(String consumerNo) {
        this.consumerNo = consumerNo;
    }

    @Override
    public BigDecimal getAverageConsumption() {
        return averageConsumption;
    }

    @Override
    public void setAverageConsumption(BigDecimal averageConsumption) {
        this.averageConsumption = averageConsumption;
    }

    @Override
    public BigDecimal getAverageBill() {
        return averageBill;
    }

    @Override
    public void setAverageBill(BigDecimal averageBill) {
        this.averageBill = averageBill;
    }

    @Override
    public boolean isDefaulter() {
        return isDefaulter;
    }

    @Override
    public void setDefaulter(boolean defaulter) {
        isDefaulter = defaulter;
    }

    @Override
    public String getBillMonth() {
        return billMonth;
    }

    @Override
    public void setBillMonth(String billMonth) {
        this.billMonth = billMonth;
    }

    @Override
    public BigDecimal getSecurityDepositDemand() {
        return securityDepositDemand;
    }

    @Override
    public void setSecurityDepositDemand(BigDecimal securityDepositDemand) {
        this.securityDepositDemand = securityDepositDemand;
    }

    @Override
    public int getPeriod() {
        return period;
    }

    @Override
    public void setPeriod(int period) {
        this.period = period;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Override
    public Date getCreatedOn() {
        return createdOn;
    }

    @Override
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Override
    public Date getUpdatedOn() {
        return updatedOn;
    }

    @Override
    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    @Override
    public String toString() {
        return "AdditionalSecurityDeposit{" +
                "id=" + id +
                ", consumerNo='" + consumerNo + '\'' +
                ", averageConsumption=" + averageConsumption +
                ", averageBill=" + averageBill +
                ", isDefaulter=" + isDefaulter +
                ", billMonth='" + billMonth + '\'' +
                ", securityDepositDemand=" + securityDepositDemand +
                ", period=" + period +
                ", createdBy='" + createdBy + '\'' +
                ", createdOn=" + createdOn +
                ", updatedBy='" + updatedBy + '\'' +
                ", updatedOn=" + updatedOn +
                '}';
    }
}
