package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.WeldingSurchargeConfigurationInterface;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity(name = "WeldingSurchargeConfiguration")
@Table(name = "welding_surcharge_configuration")
public class WeldingSurchargeConfiguration implements WeldingSurchargeConfigurationInterface{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "tariff_id")
    private long tariffId;

    @Column(name = "subcategory_code")
    private long subcategoryCode;

    @Column(name = "rate")
    private String rate;

    @Column(name = "multiplier")
    private BigDecimal multiplier;

    @Column(name = "power_factor_threshold")
    private BigDecimal powerFactorThreshold;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "created_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getTariffId() {
        return tariffId;
    }

    public void setTariffId(long tariffId) {
        this.tariffId = tariffId;
    }

    public long getSubcategoryCode() {
        return subcategoryCode;
    }

    public void setSubcategoryCode(long subcategoryCode) {
        this.subcategoryCode = subcategoryCode;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public BigDecimal getMultiplier() {
        if(this.multiplier != null){
            double temp = this.multiplier.doubleValue();
            return new BigDecimal(String.valueOf(temp));
        }
        return multiplier;
    }

    public void setMultiplier(BigDecimal multiplier) {
        this.multiplier = multiplier;
    }

    public BigDecimal getPowerFactorThreshold() {
        if(this.powerFactorThreshold != null){
            double temp = this.powerFactorThreshold.doubleValue();
            return new BigDecimal(String.valueOf(temp));
        }
        return powerFactorThreshold;
    }

    public void setPowerFactorThreshold(BigDecimal powerFactorThreshold) {
        this.powerFactorThreshold = powerFactorThreshold;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    @Override
    public String toString() {
        return "WeldingSurchargeConfiguration{" +
                "id=" + id +
                ", tariffId=" + tariffId +
                ", subcategoryCode=" + subcategoryCode +
                ", rate='" + rate + '\'' +
                ", multiplier=" + multiplier +
                ", powerFactorThreshold=" + powerFactorThreshold +
                ", createdBy='" + createdBy + '\'' +
                ", createdOn=" + createdOn +
                '}';
    }
}
