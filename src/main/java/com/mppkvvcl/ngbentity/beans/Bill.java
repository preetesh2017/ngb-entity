package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by PREETESH on 7/29/2017.
 */
@Entity(name = "Bill")
@Table(name = "bill")
public class Bill implements BillInterface {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "location_code")
    private String locationCode;

    @Column(name = "group_no")
    private String groupNo;

    @Column(name = "reading_diary_no")
    private String readingDiaryNo;

    @Column(name = "consumer_no")
    private String consumerNo;

    @Column(name = "bill_month")
    private String billMonth;

    @Column(name = "bill_date")
    @Temporal(TemporalType.DATE)
    private Date billDate;

    @Column(name = "due_date")
    @Temporal(TemporalType.DATE)
    private Date dueDate;

    @Column(name = "cheque_due_date")
    @Temporal(TemporalType.DATE)
    private Date chequeDueDate;

    @Column(name = "current_read_date")
    @Temporal(TemporalType.DATE)
    private Date currentReadDate;

    @Column(name = "current_read")
    private BigDecimal currentRead;

    @Column(name = "previous_read")
    private BigDecimal previousRead;

    @Column(name = "difference")
    private BigDecimal difference;

    @Column(name = "mf")
    private BigDecimal mf;

    @Column(name = "metered_unit")
    private BigDecimal meteredUnit;

    @Column(name = "assessment")
    private BigDecimal assessment;

    @Column(name = "total_unit")
    private BigDecimal totalUnit;

    @Column(name = "gmc_unit")
    private BigDecimal gmcUnit;

    @Column(name = "billed_unit")
    private BigDecimal billedUnit;

    @Column(name = "billed_md")
    private BigDecimal billedMD;

    @Column(name = "billed_pf")
    private BigDecimal billedPF;

    @Column(name = "load_factor")
    private BigDecimal loadFactor;

    @Column(name = "fixed_charge")
    private BigDecimal fixedCharge;

    @Column(name = "additional_fixed_charges1")
    private BigDecimal additionalFixedCharges1;

    @Column(name = "additional_fixed_charges2")
    private BigDecimal additionalFixedCharges2;

    @Column(name = "energy_charge")
    private BigDecimal energyCharge;

    @Column(name = "fca_charge")
    private BigDecimal fcaCharge;

    @Column(name = "electricity_duty")
    private BigDecimal electricityDuty;

    @Column(name = "meter_rent")
    private BigDecimal meterRent;

    @Column(name = "pf_charge")
    private BigDecimal pfCharge;

    @Column(name = "welding_transformer_surcharge")
    private BigDecimal weldingTransformerSurcharge;

    @Column(name = "load_factor_incentive")
    private BigDecimal loadFactorIncentive;

    @Column(name = "sd_interest")
    private BigDecimal sdInterest;

    @Column(name = "ccb_adjustment")
    private BigDecimal ccbAdjustment;

    @Column(name = "lock_credit")
    private BigDecimal lockCredit;

    @Column(name = "other_adjustment")
    private BigDecimal otherAdjustment;

    @Column(name = "employee_rebate")
    private BigDecimal employeeRebate;

    @Column(name = "online_payment_rebate")
    private BigDecimal onlinePaymentRebate;

    @Column(name = "prepaid_meter_rebate")
    private BigDecimal prepaidMeterRebate;

    @Column(name = "prompt_payment_incentive")
    private BigDecimal promptPaymentIncentive;

    @Column(name = "advance_payment_incentive")
    private BigDecimal advancePaymentIncentive;

    @Column(name = "demand_side_incentive")
    private BigDecimal demandSideIncentive;

    @Column(name = "subsidy")
    private BigDecimal subsidy;

    @Column(name = "current_bill")
    private BigDecimal currentBill;

    @Column(name = "arrear")
    private BigDecimal arrear;

    @Column(name = "cumulative_surcharge")
    private BigDecimal cumulativeSurcharge;

    @Column(name = "surcharge_demanded")
    private BigDecimal surchargeDemanded;

    @Column(name = "net_bill")
    private BigDecimal netBill;

    @Column(name = "asd_billed")
    private BigDecimal asdBilled;

    @Column(name = "asd_arrear")
    private BigDecimal asdArrear;

    @Column(name = "asd_installment")
    private BigDecimal asdInstallment;

    @Column(name = "deleted")
    private boolean deleted;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "created_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;

    @Column(name = "updated_by")
    private String updatedBy;

    @Column(name = "updated_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;

    @Column(name = "pristine_electricity_duty")
    private BigDecimal pristineElectricityDuty;

    @Column(name = "pristine_net_bill")
    private BigDecimal pristineNetBill;

    @Column(name = "current_bill_surcharge")
    private BigDecimal currentBillSurcharge;

    @Column(name = "bill_type_code")
    private String billTypeCode;

    @Column(name = "xray_fixed_charge")
    private BigDecimal xrayFixedCharge;

    @Transient
    private AgricultureBill6MonthlyInterface agricultureBill6Monthly;

    @Transient
    private List<PaymentInterface> paymentInterfaces;

    @Transient
    private List<AdjustmentInterface> adjustmentInterfaces;

    @Transient
    private ReadMasterInterface readMasterInterface;

    @Transient
    private SecurityDepositInterestInterface securityDepositInterestInterface;

    @Transient
    private TDSInterface tdsInterface;

    @Transient
    private GMCAccountingInterface gmcAccountingInterface;

    @Transient
    private List<BillCalculationLineInterface> billCalculationLineInterfaces;

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String getLocationCode() {
        return locationCode;
    }

    @Override
    public void setLocationCode(String locationCode) {
        this.locationCode = locationCode;
    }

    @Override
    public String getGroupNo() {
        return groupNo;
    }

    @Override
    public void setGroupNo(String groupNo) {
        this.groupNo = groupNo;
    }

    @Override
    public String getReadingDiaryNo() {
        return readingDiaryNo;
    }

    @Override
    public void setReadingDiaryNo(String readingDiaryNo) {
        this.readingDiaryNo = readingDiaryNo;
    }

    @Override
    public String getConsumerNo() {
        return consumerNo;
    }

    @Override
    public void setConsumerNo(String consumerNo) {
        this.consumerNo = consumerNo;
    }

    @Override
    public String getBillMonth() {
        return billMonth;
    }

    @Override
    public void setBillMonth(String billMonth) {
        this.billMonth = billMonth;
    }

    @Override
    public Date getBillDate() {
        return billDate;
    }

    @Override
    public void setBillDate(Date billDate) {
        this.billDate = billDate;
    }

    @Override
    public Date getDueDate() {
        return dueDate;
    }

    @Override
    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    @Override
    public Date getChequeDueDate() {
        return chequeDueDate;
    }

    @Override
    public void setChequeDueDate(Date chequeDueDate) {
        this.chequeDueDate = chequeDueDate;
    }

    @Override
    public Date getCurrentReadDate() {
        return currentReadDate;
    }

    @Override
    public void setCurrentReadDate(Date currentReadDate) {
        this.currentReadDate = currentReadDate;
    }

    @Override
    public BigDecimal getCurrentRead() {
        return currentRead;
    }

    @Override
    public void setCurrentRead(BigDecimal currentRead) {
        this.currentRead = currentRead;
    }

    @Override
    public BigDecimal getPreviousRead() {
        return previousRead;
    }

    @Override
    public void setPreviousRead(BigDecimal previousRead) {
        this.previousRead = previousRead;
    }

    @Override
    public BigDecimal getDifference() {
        return difference;
    }

    @Override
    public void setDifference(BigDecimal difference) {
        this.difference = difference;
    }

    @Override
    public BigDecimal getMf() {
        return mf;
    }

    @Override
    public void setMf(BigDecimal mf) {
        this.mf = mf;
    }

    @Override
    public BigDecimal getMeteredUnit() {
        if(this.meteredUnit != null){
            double temp = this.meteredUnit.doubleValue();
            return new BigDecimal(String.valueOf(temp));
        }
        return meteredUnit;
    }

    @Override
    public void setMeteredUnit(BigDecimal meteredUnit) {
        this.meteredUnit = meteredUnit;
    }

    @Override
    public BigDecimal getAssessment() {
        return assessment;
    }

    @Override
    public void setAssessment(BigDecimal assessment) {
        this.assessment = assessment;
    }

    @Override
    public BigDecimal getTotalUnit() {
        if(this.totalUnit != null){
            double temp = this.totalUnit.doubleValue();
            return new BigDecimal(String.valueOf(temp));
        }
        return totalUnit;
    }

    @Override
    public void setTotalUnit(BigDecimal totalUnit) {
        this.totalUnit = totalUnit;
    }

    @Override
    public BigDecimal getGmcUnit() {
        if(this.gmcUnit != null){
            double temp = this.gmcUnit.doubleValue();
            return new BigDecimal(String.valueOf(temp));
        }
        return gmcUnit;
    }

    @Override
    public void setGmcUnit(BigDecimal gmcUnit) {
        this.gmcUnit = gmcUnit;
    }

    @Override
    public BigDecimal getBilledUnit() {
        if(this.billedUnit != null){
            double temp = this.billedUnit.doubleValue();
            return new BigDecimal(String.valueOf(temp));
        }
        return billedUnit;
    }

    @Override
    public void setBilledUnit(BigDecimal billedUnit) {
        this.billedUnit = billedUnit;
    }

    @Override
    public BigDecimal getBilledMD() {
        if(this.billedMD != null){
            double temp = this.billedMD.doubleValue();
            return new BigDecimal(String.valueOf(temp));
        }
        return billedMD;
    }

    @Override
    public void setBilledMD(BigDecimal billedMD) {
        this.billedMD = billedMD;
    }

    @Override
    public BigDecimal getBilledPF() {
        if(this.billedPF != null){
            double temp = this.billedPF.doubleValue();
            return new BigDecimal(String.valueOf(temp));
        }
        return billedPF;
    }

    @Override
    public void setBilledPF(BigDecimal billedPF) {
        this.billedPF = billedPF;
    }

    @Override
    public BigDecimal getLoadFactor() {
        if(this.loadFactor != null){
            double temp = this.loadFactor.doubleValue();
            return new BigDecimal(String.valueOf(temp));
        }
        return loadFactor;
    }

    @Override
    public void setLoadFactor(BigDecimal loadFactor) {
        this.loadFactor = loadFactor;
    }

    @Override
    public BigDecimal getFixedCharge() {
        if(this.fixedCharge != null){
            double temp = this.fixedCharge.doubleValue();
            return new BigDecimal(String.valueOf(temp));
        }
        return fixedCharge;
    }

    @Override
    public void setFixedCharge(BigDecimal fixedCharge) {
        this.fixedCharge = fixedCharge;
    }

    @Override
    public BigDecimal getAdditionalFixedCharges1() {
        if(this.additionalFixedCharges1 != null){
            double temp = this.additionalFixedCharges1.doubleValue();
            return new BigDecimal(String.valueOf(temp));
        }
        return additionalFixedCharges1;
    }

    @Override
    public void setAdditionalFixedCharges1(BigDecimal additionalFixedCharges1) {
        this.additionalFixedCharges1 = additionalFixedCharges1;
    }

    @Override
    public BigDecimal getAdditionalFixedCharges2() {
        if(this.additionalFixedCharges2 != null){
            double temp = this.additionalFixedCharges2.doubleValue();
            return new BigDecimal(String.valueOf(temp));
        }
        return additionalFixedCharges2;
    }

    @Override
    public void setAdditionalFixedCharges2(BigDecimal additionalFixedCharges2) {
        this.additionalFixedCharges2 = additionalFixedCharges2;
    }

    @Override
    public BigDecimal getEnergyCharge() {
        if(this.energyCharge != null){
            double temp = this.energyCharge.doubleValue();
            return new BigDecimal(String.valueOf(temp));
        }
        return energyCharge;
    }

    @Override
    public void setEnergyCharge(BigDecimal energyCharge) {
        this.energyCharge = energyCharge;
    }

    @Override
    public BigDecimal getFcaCharge() {
        if(this.fcaCharge != null){
            double temp = this.fcaCharge.doubleValue();
            return new BigDecimal(String.valueOf(temp));
        }
        return fcaCharge;
    }

    @Override
    public void setFcaCharge(BigDecimal fcaCharge) {
        this.fcaCharge = fcaCharge;
    }

    @Override
    public BigDecimal getElectricityDuty() {
        if(this.electricityDuty != null){
            double temp = this.electricityDuty.doubleValue();
            return new BigDecimal(String.valueOf(temp));
        }
        return electricityDuty;
    }

    @Override
    public void setElectricityDuty(BigDecimal electricityDuty) {
        this.electricityDuty = electricityDuty;
    }

    @Override
    public BigDecimal getMeterRent() {
        if(this.meterRent != null){
            double temp = this.meterRent.doubleValue();
            return new BigDecimal(String.valueOf(temp));
        }
        return meterRent;
    }

    @Override
    public void setMeterRent(BigDecimal meterRent) {
        this.meterRent = meterRent;
    }

    public BigDecimal getPfCharge() {
        if(this.pfCharge != null){
            double temp = this.pfCharge.doubleValue();
            return new BigDecimal(String.valueOf(temp));
        }
        return pfCharge;
    }

    public void setPfCharge(BigDecimal pfCharge) {
        this.pfCharge = pfCharge;
    }

    @Override
    public BigDecimal getWeldingTransformerSurcharge() {
        if(this.weldingTransformerSurcharge != null){
            double temp = this.weldingTransformerSurcharge.doubleValue();
            return new BigDecimal(String.valueOf(temp));
        }
        return weldingTransformerSurcharge;
    }

    @Override
    public void setWeldingTransformerSurcharge(BigDecimal weldingTransformerSurcharge) {
        this.weldingTransformerSurcharge = weldingTransformerSurcharge;
    }

    @Override
    public BigDecimal getLoadFactorIncentive() {
        if(this.loadFactorIncentive != null){
            double temp = this.loadFactorIncentive.doubleValue();
            return new BigDecimal(String.valueOf(temp));
        }
        return loadFactorIncentive;
    }

    @Override
    public void setLoadFactorIncentive(BigDecimal loadFactorIncentive) {
        this.loadFactorIncentive = loadFactorIncentive;
    }

    @Override
    public BigDecimal getSdInterest() {
        if(this.sdInterest != null){
            double temp = this.sdInterest.doubleValue();
            return new BigDecimal(String.valueOf(temp));
        }
        return sdInterest;
    }

    @Override
    public void setSdInterest(BigDecimal sdInterest) {
        this.sdInterest = sdInterest;
    }

    @Override
    public BigDecimal getCcbAdjustment() {
        if(this.ccbAdjustment != null){
            double temp = this.ccbAdjustment.doubleValue();
            return new BigDecimal(String.valueOf(temp));
        }
        return ccbAdjustment;
    }

    @Override
    public void setCcbAdjustment(BigDecimal ccbAdjustment) {
        this.ccbAdjustment = ccbAdjustment;
    }

    @Override
    public BigDecimal getLockCredit() {
        if(this.lockCredit != null){
            double temp = this.lockCredit.doubleValue();
            return new BigDecimal(String.valueOf(temp));
        }
        return lockCredit;
    }

    @Override
    public void setLockCredit(BigDecimal lockCredit) {
        this.lockCredit = lockCredit;
    }

    @Override
    public BigDecimal getOtherAdjustment() {
        if(this.otherAdjustment != null){
            double temp = this.otherAdjustment.doubleValue();
            return new BigDecimal(String.valueOf(temp));
        }
        return otherAdjustment;
    }

    @Override
    public void setOtherAdjustment(BigDecimal otherAdjustment) {
        this.otherAdjustment = otherAdjustment;
    }

    @Override
    public BigDecimal getEmployeeRebate() {
        if(this.employeeRebate != null){
            double temp = this.employeeRebate.doubleValue();
            return new BigDecimal(String.valueOf(temp));
        }
        return employeeRebate;
    }

    @Override
    public void setEmployeeRebate(BigDecimal employeeRebate) {
        this.employeeRebate = employeeRebate;
    }

    @Override
    public BigDecimal getOnlinePaymentRebate() {
        if(this.onlinePaymentRebate != null){
            double temp = this.onlinePaymentRebate.doubleValue();
            return new BigDecimal(String.valueOf(temp));
        }
        return onlinePaymentRebate;
    }

    @Override
    public void setOnlinePaymentRebate(BigDecimal onlinePaymentRebate) {
        this.onlinePaymentRebate = onlinePaymentRebate;
    }

    @Override
    public BigDecimal getPrepaidMeterRebate() {
        if(this.prepaidMeterRebate != null){
            double temp = this.prepaidMeterRebate.doubleValue();
            return new BigDecimal(String.valueOf(temp));
        }
        return prepaidMeterRebate;
    }

    @Override
    public void setPrepaidMeterRebate(BigDecimal prepaidMeterRebate) {
        this.prepaidMeterRebate = prepaidMeterRebate;
    }

    @Override
    public BigDecimal getPromptPaymentIncentive() {
        if(this.promptPaymentIncentive != null){
            double temp = this.promptPaymentIncentive.doubleValue();
            return new BigDecimal(String.valueOf(temp));
        }
        return promptPaymentIncentive;
    }

    @Override
    public void setPromptPaymentIncentive(BigDecimal promptPaymentIncentive) {
        this.promptPaymentIncentive = promptPaymentIncentive;
    }

    @Override
    public BigDecimal getAdvancePaymentIncentive() {
        if(this.advancePaymentIncentive != null){
            double temp = this.advancePaymentIncentive.doubleValue();
            return new BigDecimal(String.valueOf(temp));
        }
        return advancePaymentIncentive;
    }

    @Override
    public void setAdvancePaymentIncentive(BigDecimal advancePaymentIncentive) {
        this.advancePaymentIncentive = advancePaymentIncentive;
    }

    @Override
    public BigDecimal getDemandSideIncentive() {
        if(this.demandSideIncentive != null){
            double temp = this.demandSideIncentive.doubleValue();
            return new BigDecimal(String.valueOf(temp));
        }
        return demandSideIncentive;
    }

    @Override
    public void setDemandSideIncentive(BigDecimal demandSideIncentive) {
        this.demandSideIncentive = demandSideIncentive;
    }

    @Override
    public BigDecimal getSubsidy() {
        if(this.subsidy != null){
            double temp = this.subsidy.doubleValue();
            return new BigDecimal(String.valueOf(temp));
        }
        return subsidy;
    }

    @Override
    public void setSubsidy(BigDecimal subsidy) {
        this.subsidy = subsidy;
    }

    @Override
    public BigDecimal getCurrentBill() {
        if(this.currentBill != null){
            double temp = this.currentBill.doubleValue();
            return new BigDecimal(String.valueOf(temp));
        }
        return currentBill;
    }

    @Override
    public void setCurrentBill(BigDecimal currentBill) {
        this.currentBill = currentBill;
    }

    @Override
    public BigDecimal getArrear() {
        if(this.arrear != null){
            double temp = this.arrear.doubleValue();
            return new BigDecimal(String.valueOf(temp));
        }
        return arrear;
    }

    @Override
    public void setArrear(BigDecimal arrear) {
        this.arrear = arrear;
    }

    @Override
    public BigDecimal getCumulativeSurcharge() {
        if(this.cumulativeSurcharge != null){
            double temp = this.cumulativeSurcharge.doubleValue();
            return new BigDecimal(String.valueOf(temp));
        }
        return cumulativeSurcharge;
    }

    @Override
    public void setCumulativeSurcharge(BigDecimal cumulativeSurcharge) {
        this.cumulativeSurcharge = cumulativeSurcharge;
    }

    @Override
    public BigDecimal getSurchargeDemanded() {
        if(this.surchargeDemanded != null){
            double temp = this.surchargeDemanded.doubleValue();
            return new BigDecimal(String.valueOf(temp));
        }
        return surchargeDemanded;
    }

    @Override
    public void setSurchargeDemanded(BigDecimal surchargeDemanded) {
        this.surchargeDemanded = surchargeDemanded;
    }

    @Override
    public BigDecimal getNetBill() {
        if(this.netBill != null){
            double temp = this.netBill.doubleValue();
            return new BigDecimal(String.valueOf(temp));
        }
        return netBill;
    }

    @Override
    public void setNetBill(BigDecimal netBill) {
        this.netBill = netBill;
    }

    @Override
    public BigDecimal getAsdBilled() {
        if(this.asdBilled != null){
            double temp = this.asdBilled.doubleValue();
            return new BigDecimal(String.valueOf(temp));
        }
        return asdBilled;
    }

    @Override
    public void setAsdBilled(BigDecimal asdBilled) {
        this.asdBilled = asdBilled;
    }

    @Override
    public BigDecimal getAsdArrear() {
        if(this.asdArrear != null){
            double temp = this.asdArrear.doubleValue();
            return new BigDecimal(String.valueOf(temp));
        }
        return asdArrear;
    }

    @Override
    public void setAsdArrear(BigDecimal asdArrear) {
        this.asdArrear = asdArrear;
    }

    @Override
    public BigDecimal getAsdInstallment() {
        if(this.asdInstallment != null){
            double temp = this.asdInstallment.doubleValue();
            return new BigDecimal(String.valueOf(temp));
        }
        return asdInstallment;
    }

    @Override
    public void setAsdInstallment(BigDecimal asdInstallment) {
        this.asdInstallment = asdInstallment;
    }

    @Override
    public boolean isDeleted() {
        return deleted;
    }

    @Override
    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public String getCreatedBy() {
        return createdBy;
    }

    @Override
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Override
    public Date getCreatedOn() {
        return createdOn;
    }

    @Override
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    @Override
    public String getUpdatedBy() {
        return updatedBy;
    }

    @Override
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Override
    public Date getUpdatedOn() {
        return updatedOn;
    }

    @Override
    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    @Override
    public BigDecimal getPristineElectricityDuty() {
        if(this.pristineElectricityDuty != null){
            double temp = this.pristineElectricityDuty.doubleValue();
            return new BigDecimal(String.valueOf(temp));
        }
        return pristineElectricityDuty;
    }

    @Override
    public void setPristineElectricityDuty(BigDecimal pristineElectricityDuty) {
        this.pristineElectricityDuty = pristineElectricityDuty;
    }

    @Override
    public BigDecimal getPristineNetBill() {
        if(this.pristineNetBill != null){
            double temp = this.pristineNetBill.doubleValue();
            return new BigDecimal(String.valueOf(temp));
        }
        return pristineNetBill;
    }

    @Override
    public void setPristineNetBill(BigDecimal pristineNetBill) {
        this.pristineNetBill = pristineNetBill;
    }

    @Override
    public BigDecimal getCurrentBillSurcharge() {
        if(this.currentBillSurcharge != null){
            double temp = this.currentBillSurcharge.doubleValue();
            return new BigDecimal(String.valueOf(temp));
        }
        return currentBillSurcharge;
    }

    @Override
    public void setCurrentBillSurcharge(BigDecimal currentBillSurcharge) {
        this.currentBillSurcharge = currentBillSurcharge;
    }

    @Override
    public void setAgricultureBill6Monthly(AgricultureBill6MonthlyInterface agricultureBill6MonthlyInterface) {
        this.agricultureBill6Monthly = agricultureBill6Monthly;
    }

    @Override
    public AgricultureBill6MonthlyInterface getAgricultureBill6Monthly() {
        return this.agricultureBill6Monthly;
    }

    @Override
    public List<PaymentInterface> getPaymentInterfaces() {
        return paymentInterfaces;
    }

    @Override
    public void setPaymentInterfaces(List<PaymentInterface> paymentInterfaces) {
        this.paymentInterfaces = paymentInterfaces;
    }

    @Override
    public List<AdjustmentInterface> getAdjustmentInterfaces() {
        return adjustmentInterfaces;
    }

    @Override
    public void setAdjustmentInterfaces(List<AdjustmentInterface> adjustmentInterfaces) {
        this.adjustmentInterfaces = adjustmentInterfaces;
    }

    @Override
    public ReadMasterInterface getReadMasterInterface() {
        return readMasterInterface;
    }

    @Override
    public void setReadMasterInterface(ReadMasterInterface readMasterInterface) {
        this.readMasterInterface = readMasterInterface;
    }

    public SecurityDepositInterestInterface getSecurityDepositInterestInterface() {
        return securityDepositInterestInterface;
    }

    public void setSecurityDepositInterestInterface(SecurityDepositInterestInterface securityDepositInterestInterface) {
        this.securityDepositInterestInterface = securityDepositInterestInterface;
    }

    public TDSInterface getTdsInterface() {
        return tdsInterface;
    }

    public void setTdsInterface(TDSInterface tdsInterface) {
        this.tdsInterface = tdsInterface;
    }

    @Override
    public GMCAccountingInterface getGmcAccountingInterface() {
        return gmcAccountingInterface;
    }

    @Override
    public void setGmcAccountingInterface(GMCAccountingInterface gmcAccountingInterface) {
        this.gmcAccountingInterface = gmcAccountingInterface;
    }

    @Override
    public List<BillCalculationLineInterface> getBillCalculationLineInterfaces() {
        return billCalculationLineInterfaces;
    }

    @Override
    public void setBillCalculationLineInterfaces(List<BillCalculationLineInterface> billCalculationLineInterfaces) {
        this.billCalculationLineInterfaces = billCalculationLineInterfaces;
    }

    @Override
    public String getBillTypeCode() {
        return billTypeCode;
    }

    @Override
    public void setBillTypeCode(String billTypeCode) {
        this.billTypeCode = billTypeCode;
    }

    @Override
    public BigDecimal getXrayFixedCharge() {
        if(this.xrayFixedCharge != null){
            double temp = this.xrayFixedCharge.doubleValue();
            return new BigDecimal(String.valueOf(temp));
        }
        return xrayFixedCharge;
    }

    @Override
    public void setXrayFixedCharge(BigDecimal xrayFixedCharge) {
        this.xrayFixedCharge = xrayFixedCharge;
    }

    @Override
    public String toString() {
        return "Bill{" +
                "id=" + id +
                ", locationCode='" + locationCode + '\'' +
                ", groupNo='" + groupNo + '\'' +
                ", readingDiaryNo='" + readingDiaryNo + '\'' +
                ", consumerNo='" + consumerNo + '\'' +
                ", billMonth='" + billMonth + '\'' +
                ", billDate=" + billDate +
                ", dueDate=" + dueDate +
                ", chequeDueDate=" + chequeDueDate +
                ", currentReadDate=" + currentReadDate +
                ", currentRead=" + currentRead +
                ", previousRead=" + previousRead +
                ", difference=" + difference +
                ", mf=" + mf +
                ", meteredUnit=" + meteredUnit +
                ", assessment=" + assessment +
                ", totalUnit=" + totalUnit +
                ", gmcUnit=" + gmcUnit +
                ", billedUnit=" + billedUnit +
                ", billedMD=" + billedMD +
                ", billedPF=" + billedPF +
                ", loadFactor=" + loadFactor +
                ", fixedCharge=" + fixedCharge +
                ", additionalFixedCharges1=" + additionalFixedCharges1 +
                ", additionalFixedCharges2=" + additionalFixedCharges2 +
                ", energyCharge=" + energyCharge +
                ", fcaCharge=" + fcaCharge +
                ", electricityDuty=" + electricityDuty +
                ", meterRent=" + meterRent +
                ", pfCharge=" + pfCharge +
                ", weldingTransformerSurcharge=" + weldingTransformerSurcharge +
                ", loadFactorIncentive=" + loadFactorIncentive +
                ", sdInterest=" + sdInterest +
                ", ccbAdjustment=" + ccbAdjustment +
                ", lockCredit=" + lockCredit +
                ", otherAdjustment=" + otherAdjustment +
                ", employeeRebate=" + employeeRebate +
                ", onlinePaymentRebate=" + onlinePaymentRebate +
                ", prepaidMeterRebate=" + prepaidMeterRebate +
                ", promptPaymentIncentive=" + promptPaymentIncentive +
                ", advancePaymentIncentive=" + advancePaymentIncentive +
                ", demandSideIncentive=" + demandSideIncentive +
                ", subsidy=" + subsidy +
                ", currentBill=" + currentBill +
                ", arrear=" + arrear +
                ", cumulativeSurcharge=" + cumulativeSurcharge +
                ", surchargeDemanded=" + surchargeDemanded +
                ", netBill=" + netBill +
                ", asdBilled=" + asdBilled +
                ", asdArrear=" + asdArrear +
                ", asdInstallment=" + asdInstallment +
                ", deleted=" + deleted +
                ", createdBy='" + createdBy + '\'' +
                ", createdOn=" + createdOn +
                ", updatedBy='" + updatedBy + '\'' +
                ", updatedOn=" + updatedOn +
                ", pristineElectricityDuty=" + pristineElectricityDuty +
                ", pristineNetBill=" + pristineNetBill +
                ", billTypeCode='" + billTypeCode + '\'' +
                '}';
    }

    public Bill(){

    }

    public Bill(BigDecimal billedUnit,BigDecimal energyCharge,BigDecimal fixedCharge,BigDecimal fcaCharge,BigDecimal pfCharge){
        this.billedUnit = billedUnit;
        this.energyCharge = energyCharge;
        this.fixedCharge = fixedCharge;
        this.fcaCharge = fcaCharge;
        this.pfCharge = pfCharge;
        //Setting currentBill as per value
        this.currentBill = this.energyCharge.add(this.fixedCharge).add(this.fcaCharge).add(this.pfCharge);
    }
}
