package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.TariffChangeMappingInterface;
import com.mppkvvcl.ngbinterface.interfaces.TariffDescriptionInterface;

import javax.persistence.*;

/**
 * Created by Vikas on 9/20/2017.
 */

@Entity(name = "TariffChangeMapping")
@Table(name = "tariff_change_mapping")
public class TariffChangeMapping implements TariffChangeMappingInterface {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "tariff_category")
    private String tariffCategory;

    @OneToOne
    @JoinColumn(name = "tariff_description_id",referencedColumnName = "id" )
    private TariffDescription tariffDescription;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTariffCategory() {
        return tariffCategory;
    }

    public void setTariffCategory(String tariffCategory) {
        this.tariffCategory = tariffCategory;
    }

    public TariffDescriptionInterface getTariffDescription() {
        return tariffDescription;
    }

    public void setTariffDescription(TariffDescriptionInterface tariffDescriptionInterface) {
        if(tariffDescriptionInterface != null && tariffDescriptionInterface instanceof TariffDescription){
            this.tariffDescription = (TariffDescription)tariffDescriptionInterface;
        }
    }

    @Override
    public String toString() {
        return "TariffChangeMapping{" +
                "id=" + id +
                ", tariffCategory='" + tariffCategory + '\'' +
                ", tariffDescription=" + tariffDescription +
                '}';
    }
}
