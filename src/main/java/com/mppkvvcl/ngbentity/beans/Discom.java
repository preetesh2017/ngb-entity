package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.DiscomInterface;

import javax.persistence.*;

/**
 * Created by SHIVANSHU on 17-07-2017.
 */
@Entity(name = "Discom")
@Table(name = "discom")
public class Discom implements DiscomInterface {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "name")
    private String name;

    public long getId() {        return id;    }

    public void setId(long id) {        this.id = id;    }

    public String getName() {        return name;    }

    public void setName(String name) {        this.name = name;    }

    @Override
    public String toString() {
        return "Discom{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
