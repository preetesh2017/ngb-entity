package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.ConsumerConnectionAreaInformationInterface;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by PREETESH on 6/7/2017.
 */
@Entity(name = "ConsumerConnectionAreaInformation")
@Table(name = "consumer_connection_area_information")
public class ConsumerConnectionAreaInformation implements ConsumerConnectionAreaInformationInterface {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "consumer_no")
    private String consumerNo;

    @Column(name = "dtr_name")
    private String dtrName;

    @Column(name = "pole_no")
    private String poleNo;

    @Column(name = "pole_latitude")
    private String poleLatitude;

    @Column(name = "pole_longitude")
    private String poleLongitude;

    @Column(name = "feeder_name")
    private String feederName;

    @Column(name = "pole_distance")
    private long poleDistance;

    @Column(name = "area_status")
    private String areaStatus;

    @Column(name = "group_no")
    private String groupNo;

    @Column(name = "reading_diary_no")
    private String readingDiaryNo;

    @Column(name = "neighbour_connection_no")
    private String neighbourConnectionNo;

    @Column(name = "survey_date")
    @Temporal(TemporalType.DATE)
    private Date surveyDate;

    @Column(name = "created_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "updated_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;

    @Column(name = "updated_by")
    private String updatedBy;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getConsumerNo() {
        return consumerNo;
    }

    public void setConsumerNo(String consumerNo) {
        this.consumerNo = consumerNo;
    }

    public String getDtrName() {
        return dtrName;
    }

    public void setDtrName(String dtrName) {
        this.dtrName = dtrName;
    }

    public String getPoleNo() {
        return poleNo;
    }

    public void setPoleNo(String poleNo) {
        this.poleNo = poleNo;
    }

    public String getPoleLatitude() {
        return poleLatitude;
    }

    public void setPoleLatitude(String poleLatitude) {
        this.poleLatitude = poleLatitude;
    }

    public String getPoleLongitude() {
        return poleLongitude;
    }

    public void setPoleLongitude(String poleLongitude) {
        this.poleLongitude = poleLongitude;
    }

    public String getFeederName() {
        return feederName;
    }

    public void setFeederName(String feederName) {
        this.feederName = feederName;
    }

    public long getPoleDistance() {
        return poleDistance;
    }

    public void setPoleDistance(long poleDistance) {
        this.poleDistance = poleDistance;
    }

    public String getAreaStatus() {
        return areaStatus;
    }

    public void setAreaStatus(String areaStatus) {
        this.areaStatus = areaStatus;
    }

    public String getGroupNo() {
        return groupNo;
    }

    public void setGroupNo(String groupNo) {
        this.groupNo = groupNo;
    }

    public String getReadingDiaryNo() {
        return readingDiaryNo;
    }

    public void setReadingDiaryNo(String readingDiaryNo) {
        this.readingDiaryNo = readingDiaryNo;
    }

    public String getNeighbourConnectionNo() {
        return neighbourConnectionNo;
    }

    public void setNeighbourConnectionNo(String neighbourConnectionNo) { this.neighbourConnectionNo = neighbourConnectionNo; }

    public Date getSurveyDate() {
        return surveyDate;
    }

    public void setSurveyDate(Date surveyDate) {
        this.surveyDate = surveyDate;
    }

    public void setCreatedOn(Date createdOn) { this.createdOn = createdOn; }

    public Date getCreatedOn() { return createdOn; }

    public void setCreatedBy(String createdBy) { this.createdBy = createdBy; }

    public String getCreatedBy() { return createdBy; }

    public void setUpdatedOn(Date updatedOn) { this.updatedOn = updatedOn; }

    public Date getUpdatedOn() { return updatedOn;   }

    public void setUpdatedBy(String updatedBy) { this.updatedBy = updatedBy;  }

    public String getUpdatedBy() { return updatedBy;  }

    @Override
    public String toString() {
        return "ConsumerConnectionAreaInformation{" +
                "id=" + id +
                ", consumerNo='" + consumerNo + '\'' +
                ", dtrName='" + dtrName + '\'' +
                ", poleNo='" + poleNo + '\'' +
                ", poleLatitude='" + poleLatitude + '\'' +
                ", poleLongitude='" + poleLongitude + '\'' +
                ", feederName='" + feederName + '\'' +
                ", poleDistance=" + poleDistance +
                ", areaStatus='" + areaStatus + '\'' +
                ", groupNo='" + groupNo + '\'' +
                ", readingDiaryNo='" + readingDiaryNo + '\'' +
                ", neighbourConnectionNo='" + neighbourConnectionNo + '\'' +
                ", surveyDate=" + surveyDate +
                ", createdOn=" + createdOn +
                ", createdBy='" + createdBy + '\'' +
                ", updatedOn=" + updatedOn +
                ", updatedBy='" + updatedBy + '\'' +
                '}';
    }
}