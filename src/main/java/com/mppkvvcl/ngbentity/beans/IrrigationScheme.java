package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.IrrigationSchemeInterface;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity(name = "IrrigationScheme")
@Table(name = "irrigation_scheme")
public class IrrigationScheme implements IrrigationSchemeInterface {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "location_code")
    private String locationCode;

    @Column(name = "consumer_no")
    private String consumerNo;

    @Column(name = "installment_bill_month")
    private String installmentBillMonth;

    @Column(name = "old_installment_bill_month")
    private String oldInstallmentBillMonth;

    @Column(name = "frozen_arrear")
    private BigDecimal frozenArrear;

    @Column(name = "installment_amount_mirash")
    private BigDecimal installmentAmountMirash;

    @Column(name = "instalment_remaining")
    private BigDecimal instalmentRemaining;

    @Column(name = "old_installment_remaining")
    private BigDecimal oldInstalmentRemaining;

    @Column(name = "balance_frozen_jun15")
    private BigDecimal balanceFrozenJun15;

    @Column(name = "installment_amount_new")
    private BigDecimal installmentAmountNew;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLocationCode() {
        return locationCode;
    }

    public void setLocationCode(String locationCode) {
        this.locationCode = locationCode;
    }

    public String getConsumerNo() {
        return consumerNo;
    }

    public void setConsumerNo(String consumerNo) {
        this.consumerNo = consumerNo;
    }

    public String getInstallmentBillMonth() {
        return installmentBillMonth;
    }

    public void setInstallmentBillMonth(String installmentBillMonth) {
        this.installmentBillMonth = installmentBillMonth;
    }

    public String getOldInstallmentBillMonth() {
        return oldInstallmentBillMonth;
    }

    public void setOldInstallmentBillMonth(String oldInstallmentBillMonth) {
        this.oldInstallmentBillMonth = oldInstallmentBillMonth;
    }

    public BigDecimal getFrozenArrear() {
        return frozenArrear;
    }

    public void setFrozenArrear(BigDecimal frozenArrear) {
        this.frozenArrear = frozenArrear;
    }

    public BigDecimal getInstallmentAmountMirash() {
        return installmentAmountMirash;
    }

    public void setInstallmentAmountMirash(BigDecimal installmentAmountMirash) {
        this.installmentAmountMirash = installmentAmountMirash;
    }

    public BigDecimal getInstalmentRemaining() {
        return instalmentRemaining;
    }

    public void setInstalmentRemaining(BigDecimal instalmentRemaining) {
        this.instalmentRemaining = instalmentRemaining;
    }

    public BigDecimal getOldInstalmentRemaining() {
        return oldInstalmentRemaining;
    }

    public void setOldInstalmentRemaining(BigDecimal oldInstalmentRemaining) {
        this.oldInstalmentRemaining = oldInstalmentRemaining;
    }

    public BigDecimal getBalanceFrozenJun15() {
        return balanceFrozenJun15;
    }

    public void setBalanceFrozenJun15(BigDecimal balanceFrozenJun15) {
        this.balanceFrozenJun15 = balanceFrozenJun15;
    }

    public BigDecimal getInstallmentAmountNew() {
        return installmentAmountNew;
    }

    public void setInstallmentAmountNew(BigDecimal installmentAmountNew) {
        this.installmentAmountNew = installmentAmountNew;
    }

    @Override
    public String toString() {
        return "IrrigationScheme [id=" + id + ", locationCode=" + locationCode + ", consumerNo=" + consumerNo + ", installmentBillMonth=" + installmentBillMonth + ", oldInstallmentBillMonth=" + oldInstallmentBillMonth + ", frozenArrear=" + frozenArrear + ", installmentAmountMirash=" + installmentAmountMirash + ", instalmentRemaining=" + instalmentRemaining + ", oldInstalmentRemaining=" + oldInstalmentRemaining + ", balanceFrozenJun15=" + balanceFrozenJun15 + ", installmentAmountNew="
                + installmentAmountNew + "]";
    }
}
