package com.mppkvvcl.ngbentity.beans;

/**
 * Created by PREETESH on 6/7/2017.
 */

import com.mppkvvcl.ngbinterface.interfaces.ConsumerConnectionMeterInformationInterface;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity(name = "ConsumerConnectionMeterInformation")
@Table(name = "consumer_connection_meter_information")
public class ConsumerConnectionMeterInformation implements ConsumerConnectionMeterInformationInterface {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "consumer_no")
    private String consumerNo;

    @Column(name = "meter_identifier")
    private String meterIdentifier;

    @Column(name = "start_read")
    private BigDecimal startRead;

    @Column(name = "has_ctr")
    private boolean hasCTR;

    @Column(name = "meter_installation_date")
    private Date meterInstallationDate;

    @Column(name = "meter_installer_name")
    private String meterInstallerName;

    @Column(name = "meter_installer_designation")
    private String meterInstallerDesignation;

    @Column(name = "has_modem")
    private boolean hasModem;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "created_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;

    @Column(name = "updated_by")
    private String updatedBy;

    @Column(name = "updated_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getConsumerNo() {
        return consumerNo;
    }

    public void setConsumerNo(String consumerNo) {
        this.consumerNo = consumerNo;
    }

    public String getMeterIdentifier() {
        return meterIdentifier;
    }

    public void setMeterIdentifier(String meterIdentifier) {
        this.meterIdentifier = meterIdentifier;
    }

    public BigDecimal getStartRead() {
        return startRead;
    }

    public void setStartRead(BigDecimal startRead) {
        this.startRead = startRead;
    }

    public boolean getHasCTR() {
        return hasCTR;
    }

    public void setHasCTR(boolean hasCTR) {
        this.hasCTR = hasCTR;
    }

    public Date getMeterInstallationDate() {
        return meterInstallationDate;
    }

    public void setMeterInstallationDate(Date meterInstallationDate) {
        this.meterInstallationDate = meterInstallationDate;
    }

    public String getMeterInstallerName() {
        return meterInstallerName;
    }

    public void setMeterInstallerName(String meterInstallerName) {
        this.meterInstallerName = meterInstallerName;
    }

    public String getMeterInstallerDesignation() {
        return meterInstallerDesignation;
    }

    public void setMeterInstallerDesignation(String meterInstallerDesignation) {
        this.meterInstallerDesignation = meterInstallerDesignation;
    }

    public boolean getHasModem() {
        return hasModem;
    }

    public void setHasModem(boolean hasModem) {
        this.hasModem = hasModem;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Override
    public String toString() {
        return "ConsumerConnectionMeterInformation{" +
                "id=" + id +
                ", consumerNo='" + consumerNo + '\'' +
                ", meterIdentifier='" + meterIdentifier + '\'' +
                ", startRead=" + startRead +
                ", hasCTR=" + hasCTR +
                ", meterInstallationDate=" + meterInstallationDate +
                ", meterInstallerName='" + meterInstallerName + '\'' +
                ", meterInstallerDesignation='" + meterInstallerDesignation + '\'' +
                ", hasModem=" + hasModem +
                ", createdBy='" + createdBy + '\'' +
                ", createdOn=" + createdOn +
                ", updatedBy='" + updatedBy + '\'' +
                ", updatedOn=" + updatedOn +
                '}';
    }
}
