package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.UnmeteredUnitInterface;

import javax.persistence.*;

@Entity(name = "UnmeteredUnit")
@Table(name = "unmetered_unit")
public class UnmeteredUnit implements UnmeteredUnitInterface {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id")
    private long id;

    @Column(name = "subcategory_code")
    private long subcategoryCode;

    @Column(name = "tariff_id")
    private long tariffId;

    @Column(name = "billing_unit")
    private long billingUnit;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getSubcategoryCode() {
        return subcategoryCode;
    }

    public void setSubcategoryCode(long subcategoryCode) {
        this.subcategoryCode = subcategoryCode;
    }

    public long getTariffId() {
        return tariffId;
    }

    public void setTariffId(long tariffId) {
        this.tariffId = tariffId;
    }

    public long getBillingUnit() {
        return billingUnit;
    }

    public void setBillingUnit(long billingUnit) {
        this.billingUnit = billingUnit;
    }

    @Override
    public String toString() {
        return "UnmeteredUnit{" +
                "id=" + id +
                ", subcategoryCode=" + subcategoryCode +
                ", tariffId=" + tariffId +
                ", billingUnit=" + billingUnit +
                '}';
    }
}
