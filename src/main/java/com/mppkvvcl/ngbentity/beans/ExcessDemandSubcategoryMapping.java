package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.ExcessDemandConfigurationInterface;
import com.mppkvvcl.ngbinterface.interfaces.ExcessDemandSubcategoryMappingInterface;

import javax.persistence.*;

@Table(name = "excess_demand_subcategory_mapping")
@Entity(name = "ExcessDemandSubcategoryMapping")
public class ExcessDemandSubcategoryMapping implements ExcessDemandSubcategoryMappingInterface{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "tariff_id")
    private long tariffId;

    @Column(name = "subcategory_code")
    private long subcategoryCode;

    @Column(name = "excess_demand_configuration_id")
    private long excessDemandConfigurationId;

    @OneToOne
    @JoinColumn(name = "excess_demand_configuration_id",referencedColumnName = "id",updatable = false,insertable = false)
    private ExcessDemandConfiguration excessDemandConfiguration;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getTariffId() {
        return tariffId;
    }

    public void setTariffId(long tariffId) {
        this.tariffId = tariffId;
    }

    public long getSubcategoryCode() {
        return subcategoryCode;
    }

    public void setSubcategoryCode(long subcategoryCode) {
        this.subcategoryCode = subcategoryCode;
    }

    public long getExcessDemandConfigurationId() {
        return excessDemandConfigurationId;
    }

    public void setExcessDemandConfigurationId(long excessDemandConfigurationId) {
        this.excessDemandConfigurationId = excessDemandConfigurationId;
    }

    public ExcessDemandConfigurationInterface getExcessDemandConfiguration() {
        return excessDemandConfiguration;
    }

    public void setExcessDemandConfiguration(ExcessDemandConfigurationInterface excessDemandConfigurationInterface) {
        if(excessDemandConfigurationInterface != null && excessDemandConfigurationInterface instanceof ExcessDemandConfiguration){
            this.excessDemandConfiguration = (ExcessDemandConfiguration) excessDemandConfigurationInterface;
        }

    }

    @Override
    public String toString() {
        return "ExcessDemandSubcategoryMapping{" +
                "id=" + id +
                ", tariffId=" + tariffId +
                ", subcategoryCode=" + subcategoryCode +
                ", excessDemandConfigurationId=" + excessDemandConfigurationId +
                ", excessDemandConfiguration=" + excessDemandConfiguration +
                '}';
    }
}
