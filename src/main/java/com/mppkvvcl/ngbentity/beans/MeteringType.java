package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.MeteringTypeInterface;

import javax.persistence.*;

/**
 * Created by SHIVANSHU on 18-07-2017.
 */

@Entity(name = "MeteringType")
@Table(name = "metering_type")
public class MeteringType implements MeteringTypeInterface {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "type")
    private String type;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


    @Override
    public String toString() {
        return "MeteringType{" +
                "id=" + id +
                ", type='" + type + '\'' +
                '}';
    }

}
